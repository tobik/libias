// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#pragma once

libias_attr_returns_nonnull
struct LibiasArray *libias_array_new(void);

libias_attr_nonnull(1)
void libias_array_cleanup(struct LibiasArray **);

libias_attr_nonnull(1)
void libias_array_append(struct LibiasArray *, const void *);

libias_attr_nonnull(1, 2, 3)
struct LibiasDiff *libias_array_diff(struct LibiasArray *, struct LibiasArray *, struct LibiasCompareTrait *);

libias_attr_nonnull(1)
void *libias_array_get(struct LibiasArray *, size_t);

libias_attr_nonnull(1, 3)
ssize_t libias_array_find(struct LibiasArray *, const void *, struct LibiasCompareTrait *);

libias_attr_nonnull(1)
void libias_array_insert(struct LibiasArray *, size_t, const void *);

libias_attr_nonnull(1)
void *libias_array_remove(struct LibiasArray *, size_t);

libias_attr_nonnull(1, 2)
void libias_array_join_arrays(struct LibiasArray *, struct LibiasArray *[], size_t);

size_t libias_array_len(struct LibiasArray *);

libias_attr_nonnull(1)
void *libias_array_pop(struct LibiasArray *);

libias_attr_nonnull(1)
void libias_array_set(struct LibiasArray *, size_t, const void *);

libias_attr_nonnull(1, 2)
void libias_array_sort(struct LibiasArray *, struct LibiasCompareTrait *);

libias_attr_nonnull(1, 4)
void libias_array_sort_slice(struct LibiasArray *, ssize_t, ssize_t, struct LibiasCompareTrait *);

libias_attr_nonnull(1)
void libias_array_truncate(struct LibiasArray *);

libias_attr_nonnull(1)
void libias_array_truncate_at(struct LibiasArray *array, size_t);

libias_attr_nonnull(1)
libias_attr_returns_nonnull
struct LibiasIterator *libias_array_iterator(struct LibiasArray *, ssize_t, ssize_t);

#define libias_array_join(ARRAY, ...) \
	libias_array_join__(ARRAY, libias_macro_gensym(array_join_), __VA_ARGS__)
#define libias_array_join__(ARRAY, ARRAYS, ...) do { \
	struct LibiasArray *ARRAYS[] = { __VA_ARGS__ }; \
	libias_array_join_arrays(ARRAY, ARRAYS, nitems(ARRAYS)); \
} while(0)

#define libias_array_foreach_slice(ARRAY, A, B, TYPE, VAR) \
	libias_iterator_foreach(libias_array_iterator(ARRAY, A, B), TYPE, VAR, TYPE, libias_macro_gensym(VAR))
#define libias_array_foreach(ARRAY, TYPE, VAR) \
	libias_array_foreach_slice(ARRAY, 0, -1, TYPE, VAR)

#define libias_scope_array(name) \
	libias_declare_variable_with_cleanup(struct LibiasArray, name, libias_array_cleanup, libias_array_new)
