// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#if HAVE_SYS_TREE
# include <sys/tree.h>
#endif
#include <stdlib.h>

#include "array.h"
#include "iterator.h"
#include "map.h"
#include "stack.h"
#include "trait/compare.h"
#include "util.h"

struct MapNode {
	RB_ENTRY(MapNode) entry;
	void *key;
	void *value;
	struct LibiasMap *map;
};

struct LibiasMap {
	RB_HEAD(MapTree, MapNode) root;
	struct LibiasCompareTrait compare;
	size_t len;
};

struct LibiasMapIterator {
	struct MapNode *current;
	size_t index;
	size_t len;
	size_t start;
};

// Prototypes
static void libias_map_iterator_cleanup(void **context_ptr);
static bool libias_map_iterator_next(struct LibiasIterator **it_ptr, size_t *index, void **key, void **value);
static void map_node_free(struct MapNode *node);
static int nodecmp(struct MapNode *e1, struct MapNode *e2);

RB_GENERATE_STATIC(MapTree, MapNode, entry, nodecmp);

void
map_node_free(struct MapNode *node)
{
	if (node == NULL) {
		return;
	}
	libias_free(node);
}

struct LibiasMap *
libias_map_new(struct LibiasCompareTrait *compare)
{
	struct LibiasMap *map = libias_alloc(struct LibiasMap);
	RB_INIT(&map->root);
	map->compare = *compare;
	return map;
}

void
libias_map_cleanup(struct LibiasMap **map_ptr)
{
	struct LibiasMap *map = *map_ptr;
	if (map) {
		libias_map_truncate(map);
		libias_free(map);
		*map_ptr = NULL;
	}
}

void
libias_map_add(struct LibiasMap *map, const void *key, const void *value)
{
	if (!libias_map_get(map, key)) {
		struct MapNode *node = libias_alloc(struct MapNode);
		node->key = (void *)key;
		node->value = (void *)value;
		node->map = map;
		RB_INSERT(MapTree, &map->root, node);
		map->len++;
	}
}

void
libias_map_replace(struct LibiasMap *map, const void *key, const void *value)
{
	struct MapNode search = { .key = (void *)key, .map = map };
	struct MapNode *node = RB_FIND(MapTree, &map->root, &search);
	if (node) {
		node->value = (void *)value;
	} else {
		struct MapNode *node = libias_alloc(struct MapNode);
		node->key = (void *)key;
		node->value = (void *)value;
		node->map = map;
		RB_INSERT(MapTree, &map->root, node);
		map->len++;
	}
}

void
libias_map_remove(struct LibiasMap *map, const void *key)
{
	struct MapNode search = { .key = (void *)key, .map = map };
	struct MapNode *node = RB_FIND(MapTree, &map->root, &search);
	if (node) {
		RB_REMOVE(MapTree, &map->root, node);
		map_node_free(node);
		map->len--;
	}
}

void *
libias_map_get(struct LibiasMap *map, const void *key)
{
	struct MapNode search = { .key = (void *)key, .map = map };
	struct MapNode *node = RB_FIND(MapTree, &map->root, &search);
	if (node) {
		return node->value;
	} else {
		return NULL;
	}
}

bool
libias_map_contains_p(struct LibiasMap *map, const void *key)
{
	return libias_map_get(map, key) != NULL;
}

size_t
libias_map_len(struct LibiasMap *map)
{
	return map->len;
}

void
libias_map_truncate(struct LibiasMap *map)
{
	struct LibiasStack *stack = libias_stack_new();
	struct MapNode *node;
	struct MapNode *next;
	for (node = RB_MIN(MapTree, &map->root); node != NULL; node = next) {
		next = RB_NEXT(MapTree, &map->root, node);
		libias_stack_push(stack, node);
	}
	while ((node = libias_stack_pop(stack))) {
		map_node_free(node);
	}
	libias_cleanup(&stack);

	RB_INIT(&map->root);
	map->len = 0;
}

struct LibiasArray *
libias_map_keys(struct LibiasMap *map)
{
	struct LibiasArray *array = libias_array_new();
	struct MapNode *node;
	RB_FOREACH(node, MapTree, &map->root) {
		libias_array_append(array, node->key);
	}
	return array;
}

struct LibiasArray *
libias_map_values(struct LibiasMap *map)
{
	struct LibiasArray *array = libias_array_new();
	struct MapNode *node;
	RB_FOREACH(node, MapTree, &map->root) {
		libias_array_append(array, node->value);
	}
	return array;
}

int
nodecmp(struct MapNode *e1, struct MapNode *e2)
{
	return e1->map->compare.compare(&e1->key, &e2->key, e1->map->compare.context);
}

struct LibiasIterator *
libias_map_iterator(struct LibiasMap *map, ssize_t a, ssize_t b)
{
	struct LibiasMapIterator *this = libias_alloc(struct LibiasMapIterator);
	this->current = RB_MIN(MapTree, &map->root);
	libias_slice_to_range(map->len, a, b, &this->start, &this->len);
	return libias_iterator_new(
		libias_map_iterator_next,
		this,
		libias_map_iterator_cleanup);
}

void
libias_map_iterator_cleanup(void **context_ptr)
{
	struct LibiasMapIterator *this = *context_ptr;
	if (this) {
		libias_free(this);
		*context_ptr = NULL;
	}
}

bool
libias_map_iterator_next(
	struct LibiasIterator **it_ptr,
	size_t *index,
	void **key,
	void **value)
{
	struct LibiasMapIterator *this = libias_iterator_context(*it_ptr);

	if (this->start > 0) {
		for (this->index = 0; this->current && this->index < this->start; this->index++) {
			this->current = RB_NEXT(MapTree, &(iter->current->map->root), this->current);
		}
		this->start = 0;
	}

	if (!this->current) {
		return false;
	}

	*index = this->index++;
	*key = this->current->key;
	*value = this->current->value;
	this->current = RB_NEXT(MapTree, &(iter->current->map->root), this->current);
	return true;
}
