// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#include <sys/param.h>
#include <ctype.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "array.h"
#include "iterator.h"
#include "mempool.h"
#include "str.h"
#include "trait/compare.h"
#include "util.h"

// Prototypes
static libias_declare_compare(casecompare_str);
static libias_declare_compare(compare_str);

// Constants
struct LibiasCompareTrait *libias_str_compare = &(struct LibiasCompareTrait){
	.compare = compare_str,
	.context = NULL,
};
struct LibiasCompareTrait *libias_str_casecompare = &(struct LibiasCompareTrait){
	.compare = casecompare_str,
	.context = NULL,
};

libias_define_compare(casecompare_str, const char, void)
{
	return strcasecmp(a, b);
}

libias_define_compare(compare_str, const char, void)
{
	return strcmp(a, b);
}

char *
libias_str_common_prefix(const char *a, const char *b)
{
	const char *ap = a;
	const char *bp = b;
	size_t i;
	for (i = 0; *ap != 0 && *bp != 0 && *ap++ == *bp++; i++);
	return libias_str_ndup(a, i);
}

char *
libias_str_dup(const char *s)
{
	char *retval = strdup(s);
	libias_panic_unless(retval, "strdup");
	return retval;
}

char *
libias_str_ndup(const char *s, const size_t n)
{
	char *retval = strndup(s, n);
	libias_panic_unless(retval, "strndup");
	return retval;
}

bool
libias_str_suffix_p(const char *s, const char *end)
{
	size_t len = strlen(end);
	if (strlen(s) < len) {
		return false;
	}
	return strncmp(s + strlen(s) - len, end, len) == 0;
}

char *
libias_str_join(struct LibiasArray *array, const char *sep)
{
	const size_t seplen = strlen(sep);
	const size_t lastindex = libias_array_len(array) - 1;

	size_t sz = 1;
	libias_array_foreach(array, const char *, s) {
		sz += strlen(s);
		if (s_index != lastindex) {
			sz += seplen;
		}
	}

	char *buf = libias_alloc_buffer(sz);
	char *p = buf;
	libias_array_foreach(array, const char *, s) {
		const size_t len = strlen(s);
		memcpy(p, s, len);
		p += len;
		if (s_index != lastindex) {
			memcpy(p, sep, seplen);
			p += seplen;
		}
	}

	return buf;
}

char *
str_map(const char *s, size_t len, int (*f)(int))
{
	char *buf = libias_alloc_buffer(len + 1);
	for (size_t i = 0; i < len && s[i] != 0; i++) {
		buf[i] = f((unsigned char)s[i]);
	}
	return buf;
}

char *
libias_str_printf(const char *format, ...)
{
	va_list ap;
	va_start(ap, format);
	char *buf = NULL;
	libias_panic_if(vasprintf(&buf, format, ap) < 0, "vasprintf");
	va_end(ap);

	return buf;
}

char *
libias_str_repeat(const char *s, const size_t n)
{
	const size_t len = strlen(s);
	const size_t sz = len * n + 1;
	char *buf = libias_alloc_buffer(sz);
	if (n > 0) {
		char *p = buf;
		for (size_t i = 0; i < n; i++) {
			memcpy(p, s, len);
			p += len;
		}
	}
	return buf;
}

char *
libias_str_slice(const char *s, const ssize_t a, const ssize_t b)
{
	size_t len = strlen(s);
	size_t start = 0;
	size_t end = 0;
	libias_slice_to_range(len, a, b, &start, &end);
	return libias_str_ndup(s + start, end - start);
}

struct LibiasArray *
libias_str_split(struct LibiasMempool *pool, const char *s, const char *sep)
{
	return libias_str_nsplit(pool, s, strlen(s), sep);
}

struct LibiasArray *
libias_str_nsplit(struct LibiasMempool *pool, const char *s, const size_t slen, const char *sep)
{
	struct LibiasArray *array = libias_mempool_array(pool);
	size_t seplen = strlen(sep);
	const char *buf = libias_mempool_take(pool, libias_str_ndup(s, slen));
	size_t buflen = slen;
	if (seplen > 0) {
		char *ptr;
		while (*buf && (ptr = strnstr(buf, sep, buflen))) {
			*ptr = 0;
			libias_array_append(array, buf);
			buf = ptr + seplen;
			buflen -= seplen;
		}
	}
	libias_array_append(array, buf);
	return array;
}

bool
libias_str_prefix_p(const char *s, const char *prefix)
{
	while (*prefix) {
        if (*s == *prefix) {
			s++;
			prefix++;
		} else {
            return false;
        }
    }
    return true;
}

char *
libias_str_trim(const char *s)
{
	return libias_str_trim_ws(s, NULL, NULL);
}

char *
libias_str_triml(const char *s)
{
	return libias_str_triml_ws(s, NULL);
}

char *
libias_str_trimr(const char *s)
{
	return libias_str_trimr_ws(s, NULL);
}

char *
libias_str_trim_ws(const char *s, char **prefix, char **suffix)
{
	const char *sp = s;
	for (; *sp && isspace((unsigned char)*sp); ++sp);
	if (prefix) {
		*prefix = libias_str_ndup(s, sp - s);
	}
	return libias_str_trimr_ws(sp, suffix);
}


char *
libias_str_triml_ws(const char *s, char **prefix)
{
	const char *sp = s;
	for (; *sp && isspace((unsigned char)*sp); ++sp);
	if (prefix) {
		*prefix = libias_str_ndup(s, sp - s);
	}
	return libias_str_dup(sp);
}

char *
libias_str_trimr_ws(const char *s, char **suffix)
{
	size_t len = strlen(s);
	while (len > 0 && isspace((unsigned char)s[len - 1])) {
		len--;
	}
	if (suffix) {
		*suffix = libias_str_dup(s + len);
	}
	return libias_str_ndup(s, len);
}
