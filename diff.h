/*
 * Copyright (c) 2013 Tatsuhiko Kubo <cubicdaiya@gmail.com>
 * Copyright (c) 2018 Kristaps Dzonsons <kristaps@bsd.lv>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#pragma once

enum LibiasDiffEditType {
	LIBIAS_DIFF_ADD,
	LIBIAS_DIFF_DELETE,
	LIBIAS_DIFF_COMMON
};

struct LibiasDiffShortestEditScript {
	enum LibiasDiffEditType type; // type of edit
	size_t originIdx; // if >0, index+1 in origin array
	size_t targetIdx; // if >0, index+1 in target array
	const void *e; // pointer to object
};

struct LibiasDiff {
	const void **lcs; // longest common subsequence
	size_t lcssz;
	struct LibiasDiffShortestEditScript *ses; // shortest edit script
	size_t sessz;
	size_t editdist; // edit distance
};

libias_attr_nonnull(1, 3, 5)
struct LibiasDiff *libias_diff(struct LibiasCompareTrait *, size_t, const void *, size_t, const void *, size_t);

libias_attr_nonnull(1)
void libias_diff_cleanup(struct LibiasDiff **);
