// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#pragma once

libias_attr_nonnull(1)
libias_attr_returns_nonnull
struct LibiasPath *libias_path_new_cstr(const char *, ...);

libias_attr_nonnull(1)
libias_attr_returns_nonnull
struct LibiasPath *libias_path_new_LibiasPath(struct LibiasPath *, ...);

#define libias_path_new(first, ...) \
	_Generic(\
		(first), \
		const char *: libias_path_new_cstr, \
		struct LibiasPath *: libias_path_new_LibiasPath) \
		(first __VA_OPT__(,) __VA_ARGS__, NULL)

libias_attr_nonnull(1)
libias_attr_returns_nonnull
struct LibiasPath *libias_path_from_cbuffer(const char *, size_t);

libias_attr_nonnull(1)
libias_attr_returns_nonnull
struct LibiasPath *libias_path_from_buffer(struct LibiasBuffer *);

struct LibiasPath *libias_path_clone(struct LibiasPath *);

libias_attr_nonnull(1)
void libias_path_cleanup(struct LibiasPath **);

libias_attr_nonnull(1)
libias_attr_returns_nonnull
const char *
libias_path_data(struct LibiasPath *);

libias_attr_nonnull(1)
size_t
libias_path_data_len(struct LibiasPath *);

// libias_attr_nonnull(1)
// bool libias_path_exists_p(struct LibiasPath *);

// libias_attr_nonnull(1)
// bool libias_path_directory_p(struct LibiasPath *);

// libias_attr_nonnull(1)
// bool libias_path_file_p(struct LibiasPath *);

// libias_attr_nonnull(1)
// bool libias_path_symlink_p(struct LibiasPath *);
