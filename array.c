// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#include <limits.h>
#include <stdlib.h>
#include <string.h>

#include "array.h"
#include "diff.h"
#include "iterator.h"
#include "trait/compare.h"
#include "util.h"

struct LibiasArray {
	void **buf;
	size_t cap;
	size_t len;
};

struct LibiasArrayIterator {
	void **buf;
	size_t i;
	size_t len;
};

// Prototypes
static void array_ensure_capacity(struct LibiasArray *array, size_t newlen);
static void libias_array_iterator_cleanup(void **context_ptr);
static bool libias_array_iterator_next(struct LibiasIterator **it_ptr, size_t *index, void **key, void **value);

// Constants
static const size_t INITIAL_ARRAY_CAP = 16;

struct LibiasArray *
libias_array_new(void)
{
	struct LibiasArray *array = libias_alloc(struct LibiasArray);

	array->cap = INITIAL_ARRAY_CAP;
	array->len = 0;
	libias_realloc_array(array->buf, array->cap);

	return array;
}

void
array_ensure_capacity(struct LibiasArray *array, size_t newlen)
{
	if (newlen < array->cap) {
		return;
	}

	size_t cap = libias_next_power_of_2(newlen);
	if (cap > array->cap) {
		libias_realloc_array(array->buf, cap);
		array->cap = cap;
	}
}

void
libias_array_append(struct LibiasArray *array, const void *v)
{
	array_ensure_capacity(array, array->len + 1);
	array->buf[array->len++] = (void *)v;
}

void
libias_array_insert(struct LibiasArray *array, size_t index, const void *v)
{
	if (index >= array->len) {
		array->len = index;
	}
	array_ensure_capacity(array, array->len + 1);
	memmove(array->buf + index + 1, array->buf + index, (array->len - index) * sizeof(*array->buf));
	array->len++;
	array->buf[index] = (void *)v;
}

void *
libias_array_remove(struct LibiasArray *array, size_t index)
{
	if (index >= array->len) {
		return NULL;
	} else if (index == array->len - 1) {
		array->len--;
		return array->buf[index];
	} else {
		void *val = array->buf[index];
		array->len--;
		memmove(array->buf + index, array->buf + index + 1, (array->len - index) * sizeof(*array->buf));
		return val;
	}
}

void
libias_array_join_arrays(
	struct LibiasArray *array,
	struct LibiasArray *arrays[],
	size_t arrayslen)
{
	for (size_t i = 0; i < arrayslen; i++) {
		struct LibiasArray *other = arrays[i];
		// save len because the other array might be this
		// array and array_append() modifies it
		size_t otherlen = other->len;
		for (size_t j = 0; j < otherlen; j++) {
			libias_array_append(array, other->buf[j]);
		}
	}
}

struct LibiasDiff *
libias_array_diff(
	struct LibiasArray *base1,
	struct LibiasArray *base2,
	struct LibiasCompareTrait *compare)
{
	struct LibiasDiff *d = libias_diff(
		compare,
		sizeof(*base1->buf),
		base1->buf, base1->len,
		base2->buf, base2->len);
	if (d) {
		return d;
	} else {  // sequence too complicated to generate
		libias_cleanup(&d);
		return NULL;
	}
}

void
libias_array_cleanup(struct LibiasArray **array_ptr)
{
	struct LibiasArray *array = *array_ptr;
	if (array == NULL) {
		return;
	}
	libias_free(array->buf);
	libias_free(array);
	*array_ptr = NULL;
}

ssize_t
libias_array_find(struct LibiasArray *array, const void *k, struct LibiasCompareTrait *compare)
{
	for (size_t i = 0; i < array->len; i++) {
		const void *v = array->buf[i];
		if (compare->compare(&v, &k, compare->context) == 0) {
			return i;
		}
	}
	return -1;
}

void *
libias_array_get(struct LibiasArray *array, size_t i)
{
	if (i < array->len) {
		return array->buf[i];
	}
	return NULL;
}

size_t
libias_array_len(struct LibiasArray *array)
{
	return array->len;
}

void *
libias_array_pop(struct LibiasArray *array)
{
	if (array->len > 0) {
		array->len--;
		return array->buf[array->len];
	}
	return NULL;
}

void
libias_array_set(struct LibiasArray *array, size_t index, const void *v)
{
	if (index >= array->len) {
		array_ensure_capacity(array, index + 1);
		array->len = index + 1;
	}
	array->buf[index] = (void *)v;
}

void
libias_array_sort(struct LibiasArray *array, struct LibiasCompareTrait *compare)
{
	libias_sort(array->buf, array->len, sizeof(*array->buf), compare);
}

void
libias_array_sort_slice(
	struct LibiasArray *array,
	ssize_t slice_start,
	ssize_t slice_end,
	struct LibiasCompareTrait *compare)
{
	size_t start = 0;
	size_t end = 0;
	libias_slice_to_range(array->len, slice_start, slice_end, &start, &end);
	libias_sort(
		array->buf + start,
		end - start,
		sizeof(*array->buf),
		compare);
}

void
libias_array_truncate(struct LibiasArray *array)
{
	array->len = 0;
}

void
libias_array_truncate_at(struct LibiasArray *array, size_t len)
{
	if (len < array->len) {
		array->len = len;
	}
}

struct LibiasIterator *
libias_array_iterator(struct LibiasArray *array, ssize_t a, ssize_t b)
{
	struct LibiasArrayIterator *this = libias_alloc(struct LibiasArrayIterator);
	this->buf = array->buf;
	libias_slice_to_range(array->len, a, b, &this->i, &this->len);
	return libias_iterator_new(
		libias_array_iterator_next,
		this,
		libias_array_iterator_cleanup);
}

void
libias_array_iterator_cleanup(void **this_ptr)
{
	struct LibiasArrayIterator *this = *this_ptr;
	if (this) {
		libias_free(this);
		*this_ptr = NULL;
	}
}

bool
libias_array_iterator_next(
	struct LibiasIterator **it_ptr,
	size_t *index,
	void **key,
	void **value)
{
	struct LibiasArrayIterator *this = libias_iterator_context(*it_ptr);
	if (this->i < this->len) {
		void *v = this->buf[this->i];
		*index = this->i++;
		*key = v;
		*value = v;
		return true;
	} else {
		return false;
	}
}
