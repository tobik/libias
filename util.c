// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#include <stdlib.h>

#include "trait/compare.h"
#include "util.h"

void
libias_slice_to_range(
	size_t len,
	ssize_t a_,
	ssize_t b_,
	size_t *start,
	size_t *end)
{
	size_t a;
	if (a_ < 0) {
		a_ = 0;
	}
	a = a_;
	if (a >= len) {
		a = len;
	}

	size_t b;
	if (b_ < 0) {
		b_ = len + b_ + 1;
		if (b_ < 0) {
			b_ = 0;
		}
	}
	b = b_;
	if (b > len) {
		b = len;
	}

	if (a > b) {
		*start = 0;
		*end = 0;
	} else {
		libias_panic_unless(a <= len, "range start is over length");
		libias_panic_unless(b <= len, "range end is over length");
		*start = a;
		*end = b;
	}
}

#if HAVE_GNU_QSORT_R

void
libias_sort(
	void *base,
	size_t nmemb,
	size_t size,
	struct LibiasCompareTrait *compare)
{
	qsort_r(base, nmemb, size, compare->compare, compare->context);
}

#else

// c.f. https://reviews.freebsd.org/D17083

static int
qsort_r_compare_wrapper(void *userdata, const void *a, const void *b)
{
	const struct LibiasCompareTrait *compare = userdata;
	return compare->compare(a, b, compare->context);
}

void
libias_sort(
	void *base,
	size_t nmemb,
	size_t size,
	struct LibiasCompareTrait *compare)
{
	qsort_r(base, nmemb, size, compare, qsort_r_compare_wrapper);
}

#endif
