// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#pragma once

struct dirent;
struct stat;

libias_attr_nonnull(1)
struct LibiasDirectory *libias_directory_open(const char *);

libias_attr_nonnull(2)
struct LibiasDirectory *libias_directory_openat(struct LibiasDirectory *, const char *);

libias_attr_nonnull(1)
void libias_directory_cleanup(struct LibiasDirectory **);

libias_attr_nonnull(1)
int libias_directory_chdir(struct LibiasDirectory *);

libias_attr_nonnull(1)
int libias_directory_fd(struct LibiasDirectory *);

libias_attr_nonnull(1, 2)
int libias_directory_stat(struct LibiasDirectory *, struct stat *);

libias_attr_nonnull(1)
struct LibiasFile *libias_file_open(const char *, int, mode_t);

libias_attr_nonnull(2)
struct LibiasFile *libias_file_openat(struct LibiasDirectory *, const char *, int, mode_t);

struct LibiasFile *libias_file_openfd(int);

libias_attr_returns_nonnull
struct LibiasFile *libias_file_memstream(void);

libias_attr_nonnull(1)
void libias_file_cleanup(struct LibiasFile **);

libias_attr_nonnull(1)
bool libias_file_can_use_colors(struct LibiasFile *);

libias_attr_nonnull(1)
int libias_file_fd(struct LibiasFile *);

libias_attr_nonnull(1)
off_t libias_file_len(struct LibiasFile *);

libias_attr_nonnull(1, 2)
ssize_t libias_file_read(struct LibiasFile *, struct LibiasBuffer *, size_t, size_t);

libias_attr_nonnull(1, 2)
libias_attr_printflike(2, 3)
int libias_file_printf(struct LibiasFile *, const char *, ...);

libias_attr_nonnull(1, 2)
ssize_t libias_file_put_buffer(struct LibiasFile *, struct LibiasBuffer *);

libias_attr_nonnull(1, 2)
ssize_t libias_file_put_string(struct LibiasFile *, const char *);

#define libias_file_puts(file, data) \
	_Generic(\
		(data), \
		struct LibiasBuffer *: libias_file_put_buffer, \
		char *: libias_file_put_string, \
		const char *: libias_file_put_string)((file), (data))

libias_attr_nonnull(1)
struct LibiasBuffer *libias_file_slurp(struct LibiasFile *);

libias_attr_nonnull(1)
off_t libias_file_rewind(struct LibiasFile *, off_t);

libias_attr_nonnull(1)
off_t libias_file_set_position(struct LibiasFile *, off_t);

libias_attr_nonnull(1, 2)
int libias_file_stat(struct LibiasFile *, struct stat *);

libias_attr_nonnull(1)
off_t libias_file_truncate(struct LibiasFile *);

libias_attr_nonnull(1)
off_t libias_file_truncate_at(struct LibiasFile *, off_t);

libias_attr_nonnull(2)
char *libias_symlink_read(struct LibiasDirectory *, const char *);

libias_attr_nonnull(2, 3)
bool libias_symlink_update(struct LibiasDirectory *, const char *, const char *, char **);

libias_attr_nonnull(1)
libias_attr_returns_nonnull
struct LibiasIterator *libias_directory_iterator(struct LibiasDirectory *, ssize_t, ssize_t);

libias_attr_nonnull(1)
libias_attr_returns_nonnull
struct LibiasIterator *libias_file_line_iterator(struct LibiasFile *, ssize_t, ssize_t);

libias_attr_nonnull(1)
bool libias_mkdirp(const char *);

#define libias_directory_foreach_slice(DIR, A, B, VAR) \
	libias_iterator_foreach(libias_directory_iterator(DIR, A, B), struct dirent *, VAR, struct dirent *, libias_macro_gensym(VAR))
#define libias_directory_foreach(DIR, VAR) \
	libias_directory_foreach_slice(DIR, 0, -1, VAR)

#define libias_file_line_foreach_slice(FILE, A, B, VAR) \
	libias_iterator_foreach(libias_file_line_iterator(FILE, A, B), char *, VAR, size_t *, VAR##_len)
#define libias_file_line_foreach(FILE, VAR) \
	libias_file_line_foreach_slice(FILE, 0, -1, VAR)
