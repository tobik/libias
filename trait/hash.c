// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#include <string.h>

#include "trait/hash.h"
#include "str.h"

#define uthash_malloc(sz) \
	libias_alloc_buffer(sz)
#define uthash_free(ptr, sz) \
	libias_free(ptr)
#define HASH_KEYCMP(a,b,n) \
	libias_hashmap_keycmp(map, (a), (b))
#include "uthash.h"

// Prototypes
static size_t libias_hash_int64_datalen_impl(const void *data_ptr, void *context);
static uint64_t libias_hash_int64_impl(const void *data_ptr, const size_t datalen, void *context);
static size_t libias_hash_ptr_datalen_impl(const void *data_ptr, void *context);
static uint64_t libias_hash_ptr_impl(const void *data_ptr, const size_t datalen, void *context);
static size_t libias_hash_str_datalen_impl(const void *data_ptr, void *context);
static uint64_t libias_hash_str_impl(const void *data_ptr, const size_t datalen, void *context);

// Constants
struct LibiasHashTrait *libias_hash_int64 = &(struct LibiasHashTrait){
	.hash = libias_hash_int64_impl,
	.datalen = libias_hash_int64_datalen_impl,
	.context = NULL,
};
struct LibiasHashTrait *libias_hash_ptr = &(struct LibiasHashTrait){
	.hash = libias_hash_ptr_impl,
	.datalen = libias_hash_ptr_datalen_impl,
	.context = NULL,
};
struct LibiasHashTrait *libias_hash_str = &(struct LibiasHashTrait){
	.hash = libias_hash_str_impl,
	.datalen = libias_hash_str_datalen_impl,
	.context = NULL,
};

uint64_t
libias_hash_generic(
	const void *data,
	const size_t datalen)
{
	uint64_t hashv;
	HASH_JEN(data, datalen, hashv);
	return hashv;
}

size_t
libias_hash_int64_datalen_impl(
	const void *data_ptr,
	void *context)
{
	return sizeof(int64_t);
}

uint64_t
libias_hash_int64_impl(
	const void *data_ptr,
	const size_t datalen,
	void *context)
{
	int64_t *i = *(int64_t **)data_ptr;
	return libias_hash_generic(i, datalen);
}

size_t
libias_hash_ptr_datalen_impl(
	const void *data_ptr,
	void *context)
{
	return sizeof(void *);
}

uint64_t
libias_hash_ptr_impl(
	const void *data_ptr,
	const size_t datalen,
	void *context)
{
	return libias_hash_generic(data_ptr, datalen);
}

size_t
libias_hash_str_datalen_impl(
	const void *data_ptr,
	void *context)
{
	const char *buf = *(const char **)data_ptr;
	return strlen(buf);
}

uint64_t
libias_hash_str_impl(
	const void *data_ptr,
	const size_t datalen,
	void *context)
{
	char *buf = *(char **)data_ptr;
	return libias_hash_generic(buf, datalen);
}
