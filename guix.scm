;; This file is waived into the public domain, as-is, no warranty provided.
;;
;; If the public domain doesn't exist where you live, consider
;; this a license which waives all copyright and neighboring intellectual
;; restrictions laws mechanisms, to the fullest extent possible by law,
;; as-is, no warranty provided.
;;
;; No attribution is required and you are free to copy-paste and munge
;; into your own project.

(define-module (tobik packages libias)
  #:use-module (gnu packages)
  #:use-module (gnu packages ninja)
  #:use-module (guix build-system gnu)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (srfi srfi-1))

(define (keep-file? file stat)
  (not (any (lambda (my-string)
              (string-contains file my-string))
            (list ".ccls-cache" ".cache" ".dir-locals.el" ".git" "_build" "guix.scm"))))

(define-public libias
  (package
    (name "libias")
    (version "0")
    (source (local-file (dirname (current-filename))
                        #:recursive? #t
                        #:select? keep-file?))
    (build-system gnu-build-system)
    (arguments
     (list #:phases
           #~(modify-phases %standard-phases
               (replace 'configure
                 (lambda _
                   (invoke "./configure"
                           ;; select target gcc-ar for LTO build if available
                           (string-append "AR="
                                          (or (which #$(string-append (cc-for-target) "-ar"))
                                              #$(ar-for-target)))
                           (string-append "CC=" #$(cc-for-target))
                           (string-append "PREFIX=" #$output)
                           (string-append "TARGET_INTERPRETER="
                                          #$(if (%current-target-system)
                                                "true"
                                                "")))))
               (replace 'build
                 (lambda* (#:key parallel-build? #:allow-other-keys)
                   (let ((job-count (if parallel-build?
                                        (number->string (parallel-job-count))
                                        "1")))
                     (invoke "ninja" "-j" job-count "-C" "_build"))))
               (replace 'install
                 (lambda _
                   (invoke "ninja" "-C" "_build" "install")))
               (replace 'check
                 (lambda* (#:key parallel-tests? #:allow-other-keys)
                   (let ((job-count (if parallel-tests?
                                        (number->string (parallel-job-count))
                                        "1")))
                     (invoke "ninja" "-j" job-count "-C" "_build" "test")))))))
    (native-inputs (list ninja))
    (synopsis "Libias")
    (description "Libias")
    (home-page "https://codeberg.org/tobik/libias")
    (license license:bsd-3)))

libias
