// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#define LIBIAS_RUNTIME_MODULE 1
#include "config.h"

#if HAVE_EXECINFO
# if HAVE_CAPSICUM
#  include <sys/capsicum.h>
# endif
# include <execinfo.h>
#endif
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

char *
libias_alloc_buffer(size_t size)
{
	char *x = calloc(1, size);
	libias_panic_unless(x, "calloc");
	return x;
}

void *
libias_runtime_realloc_array(
	void *ptr,
	size_t size,
	size_t n)
{
	size_t bytes;
	if (ckd_mul(&bytes, n, size)) {
		libias_panic("integer overflow: %zu * %zu", size, n);
	} else {
		void *x = realloc(ptr, bytes);
		libias_panic_unless(x, "realloc");
		return x;
	}
}

void
libias_free(void *x)
{
	free(x);
}

void
libias_runtime_cleanup_char_ptr(char **x)
{
	libias_runtime_cleanup((void **)x);
}

void
libias_runtime_cleanup(void **x)
{
	free(*x);
	*x = NULL;
}

void
libias_runtime_panic(const char *file, uint32_t line, const char *fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);

	fprintf(stderr, "panic: ");
	vfprintf(stderr, fmt, ap);
	fprintf(stderr, " at %s:%" PRIu32 "\n", file, line);
	va_end(ap);
#if HAVE_EXECINFO
#if HAVE_CAPSICUM
	u_int mode;
	if (cap_getmode(&mode) == 0 && mode != 0) {
		abort();
	}
#endif
	void *addrlist[100];
	size_t len = backtrace(addrlist, nitems(addrlist));
	if (len >= 1) {
		backtrace_symbols_fd(addrlist + 1, len - 1, STDERR_FILENO);
	}
#endif
	abort();
}

void
libias_runtime_scope_on_exit_cleanup(void **ptr)
{
	struct {
		const char *file;
		uint32_t line;
		const char *freefn_name;
		void *freefn;
		size_t args_len;
		void **args;
	} *this = *ptr;
	if (this) {
		switch (this->args_len) {
		case 0:
		{
			void (*freefn)(void) = this->freefn;
			freefn();
			break;
		}
		case 1:
		{
			void (*freefn)(void *) = this->freefn;
			freefn(this->args[0]);
			break;
		}
		case 2:
		{
			void (*freefn)(void *, void *) = this->freefn;
			freefn(this->args[0], this->args[1]);
			break;
		}
		case 3:
		{
			void (*freefn)(void *, void *, void *) = this->freefn;
			freefn(
				this->args[0],
				this->args[1],
				this->args[2]);
			break;
		}
		case 4:
		{
			void (*freefn)(void *, void *, void *, void *) = this->freefn;
			freefn(
				this->args[0],
				this->args[1],
				this->args[2],
				this->args[3]);
			break;
		}
		case 5:
		{
			void (*freefn)(void *, void *, void *, void *, void *) = this->freefn;
			freefn(
				this->args[0],
				this->args[1],
				this->args[2],
				this->args[3],
				this->args[4]);
			break;
		}
		case 6:
		{
			void (*freefn)(void *, void *, void *, void *, void *, void *) = this->freefn;
			freefn(
				this->args[0],
				this->args[1],
				this->args[2],
				this->args[3],
				this->args[4],
				this->args[5]);
			break;
		}
		case 7:
		{
			void (*freefn)(void *, void *, void *, void *, void *, void *, void *) = this->freefn;
			freefn(
				this->args[0],
				this->args[1],
				this->args[2],
				this->args[3],
				this->args[4],
				this->args[5],
				this->args[6]);
			break;
		}
		case 8:
		{
			void (*freefn)(void *, void *, void *, void *, void *, void *, void *, void *) = this->freefn;
			freefn(
				this->args[0],
				this->args[1],
				this->args[2],
				this->args[3],
				this->args[4],
				this->args[5],
				this->args[6],
				this->args[7]);
			break;
		}
		case 9:
		{
			void (*freefn)(void *, void *, void *, void *, void *, void *, void *, void *, void *) = this->freefn;
			freefn(
				this->args[0],
				this->args[1],
				this->args[2],
				this->args[3],
				this->args[4],
				this->args[5],
				this->args[6],
				this->args[7],
				this->args[8]);
			break;
		}
		case 10:
		{
			void (*freefn)(void *, void *, void *, void *, void *, void *, void *, void *, void *, void *) = this->freefn;
			freefn(
				this->args[0],
				this->args[1],
				this->args[2],
				this->args[3],
				this->args[4],
				this->args[5],
				this->args[6],
				this->args[7],
				this->args[8],
				this->args[9]);
			break;
		}
		case 11:
		{
			void (*freefn)(void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *) = this->freefn;
			freefn(
				this->args[0],
				this->args[1],
				this->args[2],
				this->args[3],
				this->args[4],
				this->args[5],
				this->args[6],
				this->args[7],
				this->args[8],
				this->args[9],
				this->args[10]);
			break;
		}
		case 12:
		{
			void (*freefn)(void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *) = this->freefn;
			freefn(
				this->args[0],
				this->args[1],
				this->args[2],
				this->args[3],
				this->args[4],
				this->args[5],
				this->args[6],
				this->args[7],
				this->args[8],
				this->args[9],
				this->args[10],
				this->args[11]);
			break;
		}
		case 13:
		{
			void (*freefn)(void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *) = this->freefn;
			freefn(
				this->args[0],
				this->args[1],
				this->args[2],
				this->args[3],
				this->args[4],
				this->args[5],
				this->args[6],
				this->args[7],
				this->args[8],
				this->args[9],
				this->args[10],
				this->args[11],
				this->args[12]);
			break;
		}
		case 14:
		{
			void (*freefn)(void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *) = this->freefn;
			freefn(
				this->args[0],
				this->args[1],
				this->args[2],
				this->args[3],
				this->args[4],
				this->args[5],
				this->args[6],
				this->args[7],
				this->args[8],
				this->args[9],
				this->args[10],
				this->args[11],
				this->args[12],
				this->args[13]);
			break;
		}
		case 15:
		{
			void (*freefn)(void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *, void *) = this->freefn;
			freefn(
				this->args[0],
				this->args[1],
				this->args[2],
				this->args[3],
				this->args[4],
				this->args[5],
				this->args[6],
				this->args[7],
				this->args[8],
				this->args[9],
				this->args[10],
				this->args[11],
				this->args[12],
				this->args[13],
				this->args[14]);
			break;
		}
		default:
			libias_runtime_panic(
				this->file,
				this->line,
				"too many arguments to libias_scope_on_exit(%s)",
				this->freefn_name);
		}
		*ptr = NULL;
	}
}
