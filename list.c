// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#include <stdlib.h>

#include "iterator.h"
#include "list.h"
#include "util.h"

struct LibiasListEntry {
	struct LibiasList *list;
	struct LibiasListEntry *next;
	struct LibiasListEntry *prev;
	void *value;
};

struct LibiasList {
	struct LibiasListEntry *head;
	struct LibiasListEntry *tail;
	size_t length;
};

struct LibiasListIterator {
	struct LibiasListEntry *entry;
	size_t i;
	size_t len;
};

// Prototypes
static void libias_list_iterator_cleanup(void **this_ptr);
static bool libias_list_iterator_next(struct LibiasIterator **it_ptr, size_t *index, void **key, void **value);

struct LibiasList *
libias_list_new(void)
{
	struct LibiasList *list = libias_alloc(struct LibiasList);
	return list;
}

void
libias_list_cleanup(struct LibiasList **list_ptr)
{
	struct LibiasList *list = *list_ptr;
	if (list) {
		for (struct LibiasListEntry *current = list->head;
			 current;
			 current = libias_list_entry_remove(current));
		libias_free(list);
		*list_ptr = NULL;
	}
}

struct LibiasListEntry *
libias_list_head(struct LibiasList *list)
{
	return list->head;
}

struct LibiasListEntry *
libias_list_tail(struct LibiasList *list)
{
	return list->tail;
}

size_t
libias_list_len(struct LibiasList *list)
{
	return list->length;
}

struct LibiasListEntry *
libias_list_entry_next(struct LibiasListEntry *entry)
{
	return entry->next;
}

struct LibiasListEntry *
libias_list_entry_prev(struct LibiasListEntry *entry)
{
	return entry->prev;
}

void *
libias_list_entry_value(struct LibiasListEntry *entry)
{
	return entry->value;
}

void
libias_list_entry_set_value(
	struct LibiasListEntry *entry,
	const void *value)
{
	entry->value = (void *)value;
}

struct LibiasListEntry *
libias_list_append(
	struct LibiasList *list,
	const void *value)
{
	return libias_list_insert_after(list, list->tail, value);
}

struct LibiasListEntry *
libias_list_prepend(
	struct LibiasList *list,
	const void *value)
{
	return libias_list_insert_before(list, list->head, value);
}

struct LibiasListEntry *libias_list_insert_before(
	struct LibiasList *list,
	struct LibiasListEntry *head,
	const void *value)
{
	struct LibiasListEntry *new_list_entry = libias_alloc(struct LibiasListEntry);
	new_list_entry->list = list;
	new_list_entry->value = (void *)value;
	list->length++;

	if (head) {
		new_list_entry->prev = head->prev;
		new_list_entry->next = head;
		if (new_list_entry->prev) {
			new_list_entry->prev->next = new_list_entry;
		}
		head->prev = new_list_entry;

		if (!new_list_entry->prev) {
			// The new list entry is the list's head
			list->head = new_list_entry;
		}
		if (!new_list_entry->next) {
			// The new list entry is the list's tail
			list->tail = new_list_entry;
		}
	} else {
		// This is the first list entry
		list->head = new_list_entry;
		list->tail = new_list_entry;
	}

	return new_list_entry;
}

struct LibiasListEntry *
libias_list_insert_after(
	struct LibiasList *list,
	struct LibiasListEntry *head,
	const void *value)
{
	// Insert new list entry
	struct LibiasListEntry *new_list_entry = libias_alloc(struct LibiasListEntry);
	new_list_entry->list = list;
	new_list_entry->value = (void *)value;
	list->length++;
	if (head) {
		new_list_entry->prev = head;
		new_list_entry->next = head->next;
		if (new_list_entry->next) {
			new_list_entry->next->prev = new_list_entry;
		}
		head->next = new_list_entry;

		if (!new_list_entry->prev) {
			// The new list entry is the list's head
			list->head = new_list_entry;
		}
		if (!new_list_entry->next) {
			// The new list entry is the list's tail
			list->tail = new_list_entry;
		}
	} else {
		// This is the first list entry
		list->head = new_list_entry;
		list->tail = new_list_entry;
	}

	return new_list_entry;
}


struct LibiasListEntry *
libias_list_entry_remove(struct LibiasListEntry *list_entry)
{
	struct LibiasList *list = list_entry->list;

	list->length--;

	if (list_entry->prev) {
		list_entry->prev->next = list_entry->next;
	} else {
		list->head = list_entry->next;
	}

	if (list_entry->next) {
		list_entry->next->prev = list_entry->prev;
	} else {
		list->tail = list_entry->prev;
	}

	struct LibiasListEntry *retval = list_entry->next;
	libias_free(list_entry);
	return retval;
}

struct LibiasIterator *
libias_list_iterator(struct LibiasList *list, ssize_t a, ssize_t b)
{
	struct LibiasListIterator *this = libias_alloc(struct LibiasListIterator);
	this->entry = list->head;
	libias_slice_to_range(list->length, a, b, &this->i, &this->len);

	for (size_t i = 0; this->entry && i < this->i; i++) {
		this->entry = this->entry->next;
	}

	return libias_iterator_new(
		libias_list_iterator_next,
		this,
		libias_list_iterator_cleanup);
}

void
libias_list_iterator_cleanup(void **this_ptr)
{
	struct LibiasListIterator *this = *this_ptr;
	if (this) {
		libias_free(this);
		*this_ptr = NULL;
	}
}

bool
libias_list_iterator_next(
	struct LibiasIterator **it_ptr,
	size_t *index,
	void **key,
	void **value)
{
	struct LibiasListIterator *iter = libias_iterator_context(*it_ptr);
	if (iter->entry && iter->i < iter->len) {
		*index = iter->i++;
		*key = iter->entry;
		*value = iter->entry->value;
		iter->entry = iter->entry->next;
		return true;
	} else {
		return false;
	}
}
