// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#pragma once

libias_attr_nonnull(1)
libias_attr_returns_nonnull
struct LibiasFramemap *libias_framemap_new(struct LibiasCompareTrait *);

libias_attr_nonnull(1)
void libias_framemap_cleanup(struct LibiasFramemap **);

libias_attr_nonnull(1, 2)
bool libias_framemap_contains_p(struct LibiasFramemap *, const void *);

libias_attr_nonnull(1)
libias_attr_returns_nonnull
struct LibiasMap *libias_framemap_flatten(struct LibiasFramemap *);

libias_attr_nonnull(1, 2)
void *libias_framemap_get(struct LibiasFramemap *, const void *);

libias_attr_nonnull(1)
libias_attr_returns_nonnull
struct LibiasArray *libias_framemap_keys(struct LibiasFramemap *);

libias_attr_nonnull(1)
libias_attr_returns_nonnull
struct LibiasArray *libias_framemap_values(struct LibiasFramemap *);

libias_attr_nonnull(1)
size_t libias_framemap_len(struct LibiasFramemap *);

libias_attr_nonnull(1)
void libias_framemap_pop(struct LibiasFramemap *);

libias_attr_nonnull(1)
void libias_framemap_push(struct LibiasFramemap *);

libias_attr_nonnull(1, 2)
void libias_framemap_remove(struct LibiasFramemap *, const void *);

libias_attr_nonnull(1, 2)
void libias_framemap_remove_all(struct LibiasFramemap *, const void *);

libias_attr_nonnull(1, 2, 3)
void libias_framemap_replace(struct LibiasFramemap *, const void *, const void *);

libias_attr_nonnull(1)
void libias_framemap_truncate(struct LibiasFramemap *);

libias_attr_nonnull(1)
libias_attr_returns_nonnull
struct LibiasIterator *libias_framemap_iterator(struct LibiasFramemap *, ssize_t, ssize_t);

#define libias_framemap_foreach_slice(FRAMEMAP, A, B, KEYTYPE, KEYVAR, VALTYPE, VALVAR) \
	libias_iterator_foreach(libias_framemap_iterator(FRAMEMAP, A, B), KEYTYPE, KEYVAR, VALTYPE, VALVAR)
#define libias_framemap_foreach(FRAMEMAP, ...) \
	libias_framemap_foreach_slice(FRAMEMAP, 0, -1, __VA_ARGS__)

#define libias_scope_framemap(name) \
	libias_declare_variable_with_cleanup(struct LibiasFramemap, name, libias_framemap_cleanup, libias_framemap_new)
