// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#pragma once

libias_attr_returns_nonnull
struct LibiasList *libias_list_new(void);

libias_attr_nonnull(1)
void libias_list_cleanup(struct LibiasList **);

libias_attr_nonnull(1)
size_t libias_list_len(struct LibiasList *);

libias_attr_nonnull(1)
struct LibiasListEntry *libias_list_head(struct LibiasList *);

libias_attr_nonnull(1)
struct LibiasListEntry *libias_list_tail(struct LibiasList *);

libias_attr_nonnull(1)
struct LibiasListEntry *libias_list_entry_next(struct LibiasListEntry *);

libias_attr_nonnull(1)
struct LibiasListEntry *libias_list_entry_prev(struct LibiasListEntry *);

libias_attr_nonnull(1, 2)
struct LibiasListEntry *libias_list_append(struct LibiasList *, const void *);

libias_attr_nonnull(1, 2)
struct LibiasListEntry *libias_list_prepend(struct LibiasList *, const void *);

libias_attr_nonnull(1, 3)
struct LibiasListEntry *libias_list_insert_before(struct LibiasList *, struct LibiasListEntry *, const void *);

libias_attr_nonnull(1, 3)
struct LibiasListEntry *libias_list_insert_after(struct LibiasList *, struct LibiasListEntry *, const void *);

libias_attr_nonnull(1)
struct LibiasListEntry *libias_list_entry_remove(struct LibiasListEntry *);

libias_attr_nonnull(1)
libias_attr_returns_nonnull
void *libias_list_entry_value(struct LibiasListEntry *);

libias_attr_nonnull(1, 2)
void libias_list_entry_set_value(struct LibiasListEntry *, const void *);

libias_attr_nonnull(1)
libias_attr_returns_nonnull
struct LibiasIterator *libias_list_iterator(struct LibiasList *, ssize_t, ssize_t);

#define libias_list_foreach_slice(LIST, A, B, VALTYPE, VALVAR) \
	libias_iterator_foreach(libias_list_iterator(LIST, A, B), struct LibiasListEntry *, VALVAR##_list_entry, VALTYPE, VALVAR)
#define libias_list_foreach(LIST, ...) \
	libias_list_foreach_slice(LIST, 0, -1, __VA_ARGS__)

#define libias_scope_list(name) \
	libias_declare_variable_with_cleanup(struct LibiasList, name, libias_list_cleanup, libias_list_new)
