// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#include <stdarg.h>
#include <stdlib.h>
#include <string.h>

#include "array.h"
#include "buffer.h"
#include "iterator.h"
#include "mempool.h"
#include "path.h"
#include "str.h"

struct LibiasPath {
	struct LibiasBuffer *buf;
};

// Prototypes
static void libias_path_normalize(struct LibiasPath *path);

struct LibiasPath *
libias_path_new_cstr(const char *first_component, ...)
{
	auto path = libias_alloc(struct LibiasPath);
	path->buf = libias_buffer_new();

	va_list ap;
	va_start(ap, first_component);
	libias_buffer_puts(path->buf, first_component);
	libias_buffer_puts(path->buf, "/");
	auto component = va_arg(ap, const char *);
	while (component) {
		libias_buffer_puts(path->buf, component);
		libias_buffer_puts(path->buf, "/");
		component = va_arg(ap, const char *);
	}
	va_end(ap);
	libias_path_normalize(path);
	return path;
}

struct LibiasPath *
libias_path_new_LibiasPath(struct LibiasPath *first_component, ...)
{
	auto path = libias_alloc(struct LibiasPath);
	path->buf = libias_buffer_new();

	va_list ap;
	va_start(ap, first_component);
	libias_buffer_puts(path->buf, libias_path_data(first_component));
	libias_buffer_puts(path->buf, "/");
	auto component = va_arg(ap, struct LibiasPath *);
	while (component) {
		libias_buffer_puts(path->buf, libias_path_data(component));
		libias_buffer_puts(path->buf, "/");
		component = va_arg(ap, struct LibiasPath *);
	}
	va_end(ap);

	libias_path_normalize(path);
	return path;
}

struct LibiasPath *
libias_path_from_cbuffer(
	const char *buf,
	size_t len)
{
	auto path = libias_alloc(struct LibiasPath);
	path->buf = libias_buffer_new();
	libias_buffer_write(path->buf, buf, sizeof(char), len);
	libias_path_normalize(path);
	return path;
}

struct LibiasPath *
libias_path_from_buffer(struct LibiasBuffer *buf)
{
	auto path = libias_alloc(struct LibiasPath);
	path->buf = libias_buffer_new();
	libias_buffer_write(
		path->buf,
		libias_buffer_data(buf),
		sizeof(char),
		libias_buffer_len(buf));
	libias_path_normalize(path);
	return path;
}

struct LibiasPath *
libias_path_clone(struct LibiasPath *path)
{
	return libias_path_from_buffer(path->buf);
}

void
libias_path_cleanup(struct LibiasPath **path_ptr)
{
	struct LibiasPath *path = *path_ptr;
	if (path) {
		libias_cleanup(&path->buf);
		libias_free(path);
		*path_ptr = NULL;
	}
}

const char *
libias_path_data(struct LibiasPath *path)
{
	return libias_buffer_data(path->buf);
}

size_t
libias_path_data_len(struct LibiasPath *path)
{
	return libias_buffer_len(path->buf);
}

void
libias_path_normalize(struct LibiasPath *path)
{
	libias_scope_mempool(pool);
	libias_scope_array(components);

	size_t dirs = libias_array_len(components);
	libias_array_foreach(
		libias_str_split(pool, libias_path_data(path), "/"),
		const char *, component)
	{
		if (*component) {
			if (strcmp(component, ".") == 0) {
				// skip
			} else if (strcmp(component, "..") == 0) {
				if (dirs > 0) {
					bool empty = libias_array_len(components) == 1
						&& strcmp(libias_array_get(components, 0), "") == 0;
					if (!empty) {
						dirs--;
						libias_array_pop(components);
					}
				} else {
					libias_array_append(components, component);
				}
			} else {
				dirs++;
				libias_array_append(components, component);
			}
		} else if (component_index == 0) {
			dirs++;
			libias_array_append(components, "");
		}
	}

	libias_buffer_truncate(path->buf);
	if (libias_array_len(components) == 1 && strcmp(libias_array_get(components, 0), "") == 0) {
		libias_buffer_puts(path->buf, "/");
	} else {
		size_t last_component_index = libias_array_len(components) - 1;
		libias_array_foreach(components, const char *, component) {
			libias_buffer_puts(path->buf, component);
			if (component_index != last_component_index) {
				libias_buffer_puts(path->buf, "/");
			}
		}
	}
}

#if 0
// Parse PATH and split it into an array of directories. The returned
// array and its elements are assigned to and then owned by POOL.
struct LibiasArray *
libias_path_split(
	struct LibiasPath *path,
	struct LibiasMempool *pool)
{
	//auto normalized_path = libias_path_normalize(path, NULL);
	//struct LibiasArray *components = libias_str_split(normalized_path , "/", pool);
	//free(normalized_path);
	return libias_mempool_array(pool); //components;
}
#endif
