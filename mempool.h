// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#pragma once

libias_attr_returns_nonnull
struct LibiasMempool *libias_mempool_new(void);

libias_attr_nonnull(1)
void libias_mempool_cleanup(struct LibiasMempool **);

libias_attr_nonnull(1, 2)
void libias_mempool_inherit(struct LibiasMempool *, struct LibiasMempool *);

void libias_mempool_release_all(struct LibiasMempool *);

libias_attr_nonnull(2, 3)
void *libias_mempool_add__(struct LibiasMempool *, void *, void *);

libias_attr_returns_nonnull
char *libias_mempool_alloc_buffer(struct LibiasMempool *, size_t);

libias_attr_nonnull(2)
void libias_mempool_forget(struct LibiasMempool *, void *);

libias_attr_nonnull(2)
void libias_mempool_move(struct LibiasMempool *, void *, struct LibiasMempool *);

libias_attr_nonnull(2)
void libias_mempool_release(struct LibiasMempool *, void *);

#define libias_mempool_add(pool, x, cleanup) \
	((typeof(x))libias_mempool_add__((pool), (x), (cleanup)))

#define libias_mempool_take(pool, x) \
	libias_mempool_add((pool), (x), libias_generic_cleanup_for_type(x))

#define libias_mempool_alloc(pool, type) \
	((type *)libias_mempool_add((pool), libias_alloc(type), libias_runtime_cleanup))
#define libias_mempool_array(pool) \
	libias_mempool_take((pool), libias_array_new())
#define libias_mempool_buffer(pool) \
	libias_mempool_take((pool), libias_buffer_new())
#define libias_mempool_framemap(pool, cmp) \
	libias_mempool_take((pool), libias_framemap_new(cmp))
#define libias_mempool_hashmap(pool, cmp, hash) \
	libias_mempool_take((pool), libias_hashmap_new(cmp, hash))
#define libias_mempool_hashset(pool, cmp, hash) \
	libias_mempool_take((pool), libias_hashset_new(cmp, hash))
#define libias_mempool_list(pool) \
	libias_mempool_take((pool), libias_list_new())
#define libias_mempool_map(pool, cmp) \
	libias_mempool_take((pool), libias_map_new(cmp))
#define libias_mempool_pool(pool) \
	libias_mempool_take((pool), libias_mempool_new())
#define libias_mempool_queue(pool) \
	libias_mempool_take((pool), libias_queue_new())
#define libias_mempool_set(pool, cmp) \
	libias_mempool_take((pool), libias_set_new(cmp))
#define libias_mempool_stack(pool) \
	libias_mempool_take((pool), libias_stack_new())
#define libias_mempool_workqueue(pool, nthreads) \
	libias_mempool_take((pool), libias_workqueue_new((nthreads)))

#define libias_scope_mempool(name) \
	libias_declare_variable_with_cleanup(struct LibiasMempool, name, libias_mempool_cleanup, libias_mempool_new)
