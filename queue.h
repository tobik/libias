// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#pragma once

libias_attr_returns_nonnull
struct LibiasQueue *libias_queue_new(void);

libias_attr_nonnull(1)
void libias_queue_cleanup(struct LibiasQueue **);

libias_attr_nonnull(1)
size_t libias_queue_len(struct LibiasQueue *);

libias_attr_nonnull(1, 2)
int libias_queue_contains_p(struct LibiasQueue *, const void *);

libias_attr_nonnull(1)
void *libias_queue_peek(struct LibiasQueue *);

libias_attr_nonnull(1)
void *libias_queue_pop(struct LibiasQueue *);

libias_attr_nonnull(1)
void *libias_queue_dequeue(struct LibiasQueue *);

libias_attr_nonnull(1, 2)
void libias_queue_push(struct LibiasQueue *, const void *);

libias_attr_nonnull(1)
void libias_queue_truncate(struct LibiasQueue *);

libias_attr_nonnull(1)
libias_attr_returns_nonnull
struct LibiasIterator *libias_queue_iterator(struct LibiasQueue *, ssize_t, ssize_t);

#define libias_queue_foreach_slice(QUEUE, A, B, TYPE, VAR) \
	libias_iterator_foreach(libias_queue_iterator(QUEUE, A, B), TYPE, VAR, TYPE, libias_macro_gensym(VAR))
#define libias_queue_foreach(QUEUE, TYPE, VAR) \
	libias_queue_foreach_slice(QUEUE, 0, -1, TYPE, VAR)

#define libias_scope_queue(name) \
	libias_declare_variable_with_cleanup(struct LibiasQueue, name, libias_queue_cleanup, libias_queue_new)
