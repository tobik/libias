// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#include <stdlib.h>

#include "iterator.h"
#include "queue.h"
#include "util.h"

struct QueueNode {
	struct QueueNode *next;
	struct QueueNode *prev;
	void *value;
};

struct LibiasQueue {
	size_t len;
	struct QueueNode *head;
	struct QueueNode *tail;
};

struct LibiasQueueIterator {
	struct QueueNode *current;
	size_t i;
	size_t len;
};

// Prototypes
static void libias_queue_iterator_cleanup(void **iter_ptr);
static bool libias_queue_iterator_next(struct LibiasIterator **it_ptr, size_t *index, void **key, void **value);

struct LibiasQueue *
libias_queue_new(void)
{
	struct LibiasQueue *queue = libias_alloc(struct LibiasQueue);
	return queue;
}

void
libias_queue_cleanup(struct LibiasQueue **queue_ptr)
{
	struct LibiasQueue *queue = *queue_ptr;
	if (!queue) {
		return;
	}

	struct QueueNode *node = queue->head;
	while (node) {
		struct QueueNode *next = node->next;
		libias_free(node);
		node = next;
	}
	libias_free(queue);
	*queue_ptr = NULL;
}

size_t
libias_queue_len(struct LibiasQueue *queue)
{
	return queue->len;
}

int
libias_queue_contains_p(struct LibiasQueue *queue, const void *value)
{
	for (struct QueueNode *node = queue->head; node; node = node->next) {
		if (value == node->value) {
			return 1;
		}
	}
	return 0;
}

void *
libias_queue_peek(struct LibiasQueue *queue)
{
	if (queue->head) {
		return queue->head->value;
	} else {
		return NULL;
	}
}

void *
libias_queue_pop(struct LibiasQueue *queue)
{
	if (queue->head) {
		void *value = queue->head->value;
		struct QueueNode *next = queue->head->next;
		libias_free(queue->head);
		queue->head = next;
		if (queue->head) {
			queue->head->prev = NULL;
			if (!queue->head->next) {
				queue->tail = queue->head;
			}
		} else {
			queue->tail = NULL;
		}
		queue->len--;
		return value;
	} else {
		return NULL;
	}
}

void
libias_queue_push(struct LibiasQueue *queue, const void *value)
{
	struct QueueNode *node = libias_alloc(struct QueueNode);
	node->value = (void *)value;
	if (queue->tail) {
		queue->tail->next = node;
		node->prev = queue->tail;
		queue->tail = node;
	} else {
		queue->head = queue->tail = node;
	}
	queue->len++;
}

void *
libias_queue_dequeue(struct LibiasQueue *queue)
{
	if (queue->tail) {
		void *value = queue->tail->value;
		struct QueueNode *newtail = queue->tail->prev;
		if (newtail) {
			newtail->next = NULL;
		}
		if (queue->tail == queue->head) {
			queue->head = newtail;
		}
		libias_free(queue->tail);
		queue->tail = newtail;
		queue->len--;
		return value;
	} else {
		return NULL;
	}
}

void
libias_queue_truncate(struct LibiasQueue *queue)
{
	struct QueueNode *node = queue->head;
	while (node) {
		struct QueueNode *next = node->next;
		libias_free(node);
		node = next;
	}
	queue->head = NULL;
	queue->tail = NULL;
	queue->len = 0;
}

struct LibiasIterator *
libias_queue_iterator(struct LibiasQueue *queue, ssize_t a, ssize_t b)
{
	struct LibiasQueueIterator *iter = libias_alloc(struct LibiasQueueIterator);
	libias_slice_to_range(queue->len, a, b, &iter->i, &iter->len);
	iter->current = queue->head;
	for (size_t i = 0; i < iter->i; i++) {
		iter->current = iter->current->next;
	}
	return libias_iterator_new(
		libias_queue_iterator_next,
		iter,
		libias_queue_iterator_cleanup);
}

void
libias_queue_iterator_cleanup(void **iter_ptr)
{
	struct LibiasQueueIterator *iter = *iter_ptr;
	if (iter) {
		libias_free(iter);
		*iter_ptr = NULL;
	}
}

bool
libias_queue_iterator_next(
	struct LibiasIterator **it_ptr,
	size_t *index,
	void **key,
	void **value)
{
	struct LibiasQueueIterator *iter = libias_iterator_context(*it_ptr);
	if (iter->i < iter->len) {
		void *v = iter->current->value;
		iter->current = iter->current->next;
		*index = iter->i++;
		*key = v;
		*value = v;
		return true;
	} else {
		return false;
	}
}
