// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#include <stdlib.h>

#include "iterator.h"
#include "framemap.h"
#include "map.h"
#include "set.h"
#include "stack.h"
#include "trait/compare.h"

struct LibiasFramemap {
	struct LibiasCompareTrait cmp;
	struct LibiasStack *stack;
};

struct LibiasFramemapIterator {
	struct LibiasMap *map;
	struct LibiasIterator *iter;
};

struct Frame {
	struct LibiasMap *map;
};

// Prototypes
static void frame_free(struct Frame *frame);
static void libias_framemap_iterator_cleanup(void **iter_ptr);
static bool libias_framemap_iterator_next(struct LibiasIterator **it_ptr, size_t *index, void **key, void **value);

// Constants
static const void *REMOVED = frame_free;

void
frame_free(struct Frame *frame)
{
	if (frame) {
		libias_cleanup(&frame->map);
		libias_free(frame);
	}
}

struct LibiasFramemap *
libias_framemap_new(struct LibiasCompareTrait *cmp)
{
	struct LibiasFramemap *map = libias_alloc(struct LibiasFramemap);
	map->cmp = *cmp;
	map->stack = libias_stack_new();
	libias_framemap_push(map);
	return map;
}

void
libias_framemap_cleanup(struct LibiasFramemap **framemap_ptr)
{
	struct LibiasFramemap *framemap = *framemap_ptr;
	if (framemap) {
		libias_stack_foreach(framemap->stack, struct Frame *, frame) {
			frame_free(frame);
		}
		libias_cleanup(&framemap->stack);
		libias_free(framemap);
		*framemap_ptr = NULL;
	}
}

void
libias_framemap_replace(struct LibiasFramemap *map, const void *key, const void *value)
{
	struct Frame *frame = libias_stack_peek(map->stack);
	libias_map_replace(frame->map, key, value);
}

void
libias_framemap_remove(struct LibiasFramemap *map, const void *key)
{
	struct Frame *frame = libias_stack_peek(map->stack);
	libias_map_replace(frame->map, key, REMOVED);
}

void
libias_framemap_remove_all(struct LibiasFramemap *map, const void *key)
{
	libias_stack_foreach(map->stack, struct Frame *, frame) {
		libias_map_remove(frame->map, key);
	}
}

void *
libias_framemap_get(struct LibiasFramemap *map, const void *key)
{
	libias_stack_foreach(map->stack, struct Frame *, frame) {
		void *val = libias_map_get(frame->map, key);
		if (val && val != REMOVED) {
			return val;
		}
	}
	return NULL;
}

bool
libias_framemap_contains_p(struct LibiasFramemap *map, const void *key)
{
	return libias_framemap_get(map, key) != NULL;
}

void
libias_framemap_push(struct LibiasFramemap *map)
{
	struct Frame *frame = libias_alloc(struct Frame);
	frame->map = libias_map_new(&map->cmp);
	libias_stack_push(map->stack, frame);
}

void
libias_framemap_pop(struct LibiasFramemap *map)
{
	if (libias_stack_len(map->stack) > 1) {
		frame_free(libias_stack_pop(map->stack));
	}
}

size_t
libias_framemap_len(struct LibiasFramemap *map)
{
	struct LibiasMap *flatmap = libias_framemap_flatten(map);
	size_t len = libias_map_len(flatmap);
	libias_cleanup(&flatmap);
	return len;
}

struct LibiasMap *
libias_framemap_flatten(struct LibiasFramemap *map)
{
	struct LibiasMap *flatmap = libias_map_new(&map->cmp);
	libias_scope_set(removed, &map->cmp);
	libias_stack_foreach(map->stack, struct Frame *, frame) {
		libias_map_foreach(frame->map, const void *, key, const void *, value) {
			if (value == REMOVED) {
				libias_set_add(removed, key);
			} else if (!libias_set_contains_p(removed, key)) {
				libias_map_add(flatmap, key, value);
			}
		}
	}
	return flatmap;
}

struct LibiasArray *
libias_framemap_keys(struct LibiasFramemap *map)
{
	struct LibiasMap *flatmap = libias_framemap_flatten(map);
	struct LibiasArray *keys = libias_map_keys(flatmap);
	libias_cleanup(&flatmap);
	return keys;
}

struct LibiasArray *
libias_framemap_values(struct LibiasFramemap *map)
{
	struct LibiasMap *flatmap = libias_framemap_flatten(map);
	struct LibiasArray *values = libias_map_values(flatmap);
	libias_cleanup(&flatmap);
	return values;
}

void
libias_framemap_truncate(struct LibiasFramemap *map)
{
	libias_stack_foreach(map->stack, struct Frame *, frame) {
		frame_free(frame);
	}
	libias_framemap_push(map);
}

struct LibiasIterator *
libias_framemap_iterator(struct LibiasFramemap *map, ssize_t a, ssize_t b)
{
	struct LibiasFramemapIterator *iter = libias_alloc(struct LibiasFramemapIterator);
	iter->map = libias_framemap_flatten(map);
	iter->iter = libias_map_iterator(iter->map, a, b);
	return libias_iterator_new(
		libias_framemap_iterator_next,
		iter,
		libias_framemap_iterator_cleanup);
}

void
libias_framemap_iterator_cleanup(void **iter_ptr)
{
	struct LibiasFramemapIterator *iter = *iter_ptr;
	if (iter) {
		libias_cleanup(&iter->iter);
		libias_cleanup(&iter->map);
		libias_free(iter);
		*iter_ptr = NULL;
	}
}

bool
libias_framemap_iterator_next(
	struct LibiasIterator **it_ptr,
	size_t *index,
	void **key,
	void **value)
{
	struct LibiasFramemapIterator *iter = libias_iterator_context(*it_ptr);
	if (libias_iterator_next(&iter->iter, index, key, value)) {
		return true;
	} else {
		return false;
	}
}
