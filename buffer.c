// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#include <sys/param.h>
#include <sys/stat.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "buffer.h"
#include "mempool.h"
#include "str.h"
#include "util.h"

struct LibiasBuffer {
	char *buf;
	size_t len;
	size_t cap;
};

// Prototypes
static void libias_buffer_ensure_capacity(struct LibiasBuffer *buffer, size_t newlen);

// Constants
static char libias_buffer_empty[] = "";

struct LibiasBuffer *
libias_buffer_new(void)
{
	struct LibiasBuffer *buffer = libias_alloc(struct LibiasBuffer);
	return buffer;
}

void
libias_buffer_ensure_capacity(
	struct LibiasBuffer *buffer,
	size_t newlen)
{
	newlen++;
	if (newlen < buffer->cap) {
		return;
	}

	size_t cap = libias_next_power_of_2(newlen);
	if (cap > buffer->cap) {
		libias_realloc_array(buffer->buf, cap);
		buffer->cap = cap;
	}
}

void
libias_buffer_cleanup(struct LibiasBuffer **buffer_ptr)
{
	struct LibiasBuffer *buffer = *buffer_ptr;
	if (buffer) {
		libias_free(buffer->buf);
		libias_free(buffer);
		*buffer_ptr = NULL;
	}
}

void
libias_buffer_write(
	struct LibiasBuffer *buffer,
	const void *buf,
	size_t size,
	size_t nmemb)
{
	size_t len;
	if (ckd_mul(&len, size, nmemb)) {
		libias_panic("integer overflow: %zu * %zu", size, nmemb);
	}

	libias_buffer_ensure_capacity(buffer, buffer->len + len);
	memcpy(buffer->buf + buffer->len, buf, len);
	buffer->len += len;
	buffer->buf[buffer->len] = 0;
}

void
libias_buffer_puts_buffer(
	struct LibiasBuffer *buffer,
	struct LibiasBuffer *other)
{
	size_t len = libias_buffer_len(other);
	char *buf = libias_buffer_data(other);
	libias_buffer_write(buffer, buf, len, 1);
}

void
libias_buffer_puts_string(
	struct LibiasBuffer *buffer,
	const char *s)
{
	libias_buffer_write(buffer, s, strlen(s), 1);
}

void
libias_buffer_puts_int64(
	struct LibiasBuffer *buffer,
	int64_t i)
{
	char *buf;
	int length = asprintf(&buf, "%ld", i);
	libias_panic_if(length < 0, "asprintf");
	libias_buffer_write(buffer, buf, length, 1);
	libias_free(buf);
}

void
libias_buffer_puts_int32(
	struct LibiasBuffer *buffer,
	int32_t i)
{
	char *buf;
	int length = asprintf(&buf, "%d", i);
	libias_panic_if(length < 0, "asprintf");
	libias_buffer_write(buffer, buf, length, 1);
	libias_free(buf);
}

void
libias_buffer_puts_size(
	struct LibiasBuffer *buffer,
	size_t sz)
{
	char *buf;
	int length = asprintf(&buf, "%zu", sz);
	libias_panic_if(length < 0, "asprintf");
	libias_buffer_write(buffer, buf, length, 1);
	libias_free(buf);
}

void
libias_buffer_puts_ssize(
	struct LibiasBuffer *buffer,
	ssize_t sz)
{
	char *buf;
	int length = asprintf(&buf, "%zi", sz);
	libias_panic_if(length < 0, "asprintf");
	libias_buffer_write(buffer, buf, length, 1);
	libias_free(buf);
}

size_t
libias_buffer_len(struct LibiasBuffer *buffer)
{
	return buffer->len;
}

void
libias_buffer_truncate(struct LibiasBuffer *buffer)
{
	libias_buffer_truncate_at(buffer, 0);
}

void
libias_buffer_truncate_at(
	struct LibiasBuffer *buffer,
	size_t n)
{
	if (n < buffer->len) {
		if (buffer->buf) {
			buffer->buf[n] = 0;
		}
		buffer->len = 0;
	}
}

// Return pointer to the buffer data. The size of the buffer can be
// requested via libias_buffer_len(). The data is owned by BUFFER. Any
// subsequent operation on BUFFER may invalidate the data and this
// function would need to be called again. The data is guaranteed to be
// zero terminated. Modifying the data directly is allowed within the
// bounds given by libias_buffer_len().
char *
libias_buffer_data(struct LibiasBuffer *buffer)
{
	if (buffer->buf) {
		return buffer->buf;
	} else {
		return libias_buffer_empty;
	}
}

// Return a copy of the BUFFER's data. The caller must free the return
// value.
char *
libias_buffer_data_copy(struct LibiasBuffer *buffer)
{
	char *buf = libias_alloc_buffer(buffer->len + 1);
	if (buffer->buf) {
		memcpy(buf, buffer->buf, buffer->len + 1);
	}
	return buf;
}

// Return the BUFFER's data. Ownership of it is transferred to the
// caller who must free it. The BUFFER will be reinitalized and is empty
// afterwards.
char *
libias_buffer_data_disown(struct LibiasBuffer *buffer)
{
	char *buf;
	if (buffer->buf) {
		buf = buffer->buf;
	} else {
		buf = libias_alloc_buffer(1);
	}
	buffer->cap = 0;
	buffer->buf = NULL;
	buffer->len = 0;
	return buf;
}
