#!/bin/sh
set -eu

: ${AWK:=awk}
: ${MV:=mv}

for file in "$@"; do
	comment="//"
	case "${file##*.}" in
		awk|sh) comment="#" ;;
		1) comment='.\\"' ;;
	esac
	${AWK} -vcomment="${comment}" '
{
	if ($0 == "") {
		print comment
	} else {
		print comment, $0
	}
}' "$(dirname "$0")/../LICENSE"> ${file}.___licensed
	${AWK} -vcomment="${comment}" '
{
	if (!after_header && $1 == comment) {
		next
	}

	if (comment != substr($0, 1, length(comment))) {
		after_header = 1
	}

	if (after_header) {
		print
	}
}
' ${file} >>${file}.___licensed
	${MV} ${file}.___licensed ${file}
done
