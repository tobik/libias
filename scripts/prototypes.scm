#!/usr/bin/env -S guix shell universal-ctags guile guile-json -- guile --no-auto-compile
!#
(define-module (tobik devhelper prototypes)
  #:use-module (json)
  #:use-module (ice-9 match)
  #:use-module (ice-9 popen)
  #:use-module (srfi srfi-26)
  #:use-module (srfi srfi-171)
  #:use-module (rnrs io ports))

(define (read-tag port)
  (catch 'json-invalid
    (lambda () (json->scm port
                     #:concatenated #t
                     #:null #f))
    (lambda (_ port) (eof-object))))

(define (uctags . args)
  (apply open-pipe* OPEN_READ "ctags" "--output-format=json" "--sort=no"
         "-Dlibias_define_compare(x, ...)=static int DEFINE_COMPARE__##x(...)"
         "-DMACHINE(x, ...)=static void MACHINE__##x(__VA_ARGS__)"
         args))

(define (sanitize-typeref typeref)
  (cond
   ((not typeref) #f)
   ((string-prefix? "struct:" typeref)
    (string-append "struct " (substring typeref (string-length "struct:"))))
   ((string-prefix? "enum:" typeref)
    (string-append "enum " (substring typeref (string-length "enum:"))))
   ((string-prefix? "typename:" typeref)
    (substring typeref (string-length "typename:")))
   (else
    typeref)))

(define (type-name tag)
  (let* ((name (assoc-ref tag "name"))
         (typeref (sanitize-typeref (assoc-ref tag "typeref"))))
    (cond
     ((not typeref) #f)
     ((string=? typeref "EXPAND_MODIFIER_PARSER_ARGS")
      typeref)
     ((string-suffix? "*" typeref)
      (string-append typeref name))
     ((string-suffix? "* []" typeref)
      (string-append (substring typeref 0 (- (string-length typeref)
                                             (string-length " []")))
                     name
                     "[]"))
     (else
      (string-append typeref " " name)))))

(define (print-prototypes module)
  (define public-functions-xform
    (tfilter-map (cut assoc-ref <> "name")))
  (define public-functions
    (let ((header (string-append (substring module 0 (- (string-length module) 2)) ".h")))
      (if (file-exists? header)
          (call-with-port (uctags "--c-kinds=p" header)
            (cut port-transduce public-functions-xform rcons read-tag <>))
          '())))

  (define functions+return-types-xform
    (compose (tremove (lambda (tag) (member (assoc-ref tag "name") public-functions)))
             (tmap (lambda (tag)
                     (let ((name (assoc-ref tag "name"))
                           (typeref (assoc-ref tag "typeref")))
                       (cons name (sanitize-typeref typeref)))))
             (tremove (match-lambda
                        ((name . _)
                         (or (string=? name "PARSER_EDIT")
                             (string=? name "portscan_main")
                             (string=? name "main")))))))
  (define functions+return-types
    (call-with-port (uctags "--c-kinds=f" module)
      (cut port-transduce functions+return-types-xform rcons read-tag <>)))

  (define function-parameters-xform
    (compose (tremove (lambda (tag) (member (assoc-ref tag "scope") public-functions)))
             (tpartition (cut assoc-ref <> "scope"))
             (tmap (lambda (tag-group)
                     (let ((scope (assoc-ref (car tag-group) "scope"))
                           (xform (tmap type-name)))
                       (cons scope (list-transduce xform rcons tag-group)))))))
  (define function-parameters
    (call-with-port (uctags "--c-kinds=z" module)
      (cut port-transduce function-parameters-xform rcons read-tag <>)))

  (for-each (match-lambda
              ((name . return-type)
               (let* ((compare? (string-prefix? "DEFINE_COMPARE__" name))
                      (machine? (string-prefix? "MACHINE__" name))
                      (args (assoc-ref function-parameters name))
                      (args (cond
                             (compare?
                              (list (substring name (string-length "DEFINE_COMPARE__"))))
                             (machine?
                              `(,(substring name (string-length "MACHINE__"))
                                ,@(if args args '())))
                             ((not args)
                              (list "void"))
                             (else args)))
                      (name (cond
                             (compare? "libias_declare_compare")
                             (machine? "MACHINE")
                             (else name)))
                      (return-type (cond
                                    (compare? "")
                                    (machine? "")
                                    ((and return-type (string-suffix? "*" return-type))
                                     return-type)
                                    (else
                                     (string-append return-type " ")))))
                 (format #t "static ~a~a(~a);\n"
                         return-type
                         name
                         (string-join args ", ")))))
            (sort functions+return-types
                  (lambda (a b) (string< (car a) (car b))))))

(for-each print-prototypes (cdr (program-arguments)))
