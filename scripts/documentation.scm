#!/usr/bin/env -S guix shell universal-ctags guile guile-json -- guile --no-auto-compile
!#
(define-module (tobik devhelper documentation)
  #:use-module (json)
  #:use-module (ice-9 match)
  #:use-module (ice-9 popen)
  #:use-module (ice-9 regex)
  #:use-module (ice-9 rdelim)
  #:use-module (srfi srfi-26)
  #:use-module (srfi srfi-171)
  #:use-module (rnrs io ports))

(define (read-tag port)
  (catch 'json-invalid
    (lambda () (json->scm port
                     #:concatenated #t
                     #:null #f))
    (lambda (_ port) (eof-object))))

(define (uctags . args)
  (apply open-pipe* OPEN_READ "ctags" "--output-format=json" "--sort=no"
         "-Dlibias_define_compare(x, ...)=static int DEFINE_COMPARE__##x(...)"
         "-DMACHINE(x, ...)=static void MACHINE__##x(__VA_ARGS__)"
         args))

(define (sanitize-typeref typeref)
  (cond
   ((not typeref) #f)
   ((string-prefix? "struct:" typeref)
    (string-append "struct " (substring typeref (string-length "struct:"))))
   ((string-prefix? "enum:" typeref)
    (string-append "enum " (substring typeref (string-length "enum:"))))
   ((string-prefix? "typename:" typeref)
    (substring typeref (string-length "typename:")))
   (else
    typeref)))

(define (type-name tag)
  (let* ((name (assoc-ref tag "name"))
         (typeref (sanitize-typeref (assoc-ref tag "typeref"))))
    (cond
     ((not typeref) #f)
     ((string=? typeref "EXPAND_MODIFIER_PARSER_ARGS")
      typeref)
     ((string-suffix? "*" typeref)
      (string-append typeref name))
     ((string-suffix? "* []" typeref)
      (string-append (substring typeref 0 (- (string-length typeref)
                                             (string-length " []")))
                     name
                     "[]"))
     (else
      (string-append typeref " " name)))))

(define (generate-manual module)
  (define module-lines
	(call-with-input-file module
	  (lambda (port)
		(let loop ((line (read-line port))
				   (lines '()))
		  (if (eof-object? line)
			  (list->vector (reverse lines))
			  (loop (read-line port)
					(cons line lines)))))))

  (define (find-documentation line-number)
	(let loop ((line-number (1- line-number))
			   (lines '()))
	  (if (>= line-number 0)
		  (let ((line (string-trim (vector-ref module-lines line-number))))
			(cond
			 ((string=? line "") lines)
			 ((string-prefix? "//" line)
			  (loop (1- line-number)
					(cons (string-trim (substring line (string-length "//")))
						  lines)))
			 (else (loop (1- line-number) lines))))
		  lines)))

  (define public-functions-xform
    (tfilter-map (cut assoc-ref <> "name")))
  (define public-functions
    (let ((header (string-append (substring module 0 (- (string-length module) 2)) ".h")))
      (if (file-exists? header)
          (call-with-port (uctags "--c-kinds=p" header)
            (cut port-transduce public-functions-xform rcons read-tag <>))
          '())))

  (define functions+return-types-xform
    (compose (tfilter (lambda (tag) (member (assoc-ref tag "name") public-functions)))
             (tmap (lambda (tag)
                     (let ((name (assoc-ref tag "name"))
                           (typeref (assoc-ref tag "typeref"))
						   (pattern (assoc-ref tag "pattern")))
                       (cons name (sanitize-typeref typeref)))))
             (tremove (match-lambda
                        ((name . _)
                         (or (string=? name "PARSER_EDIT")
                             (string=? name "portscan_main")
                             (string=? name "main")))))))
  (define functions+return-types
    (call-with-port (uctags "--c-kinds=f" module)
      (cut port-transduce functions+return-types-xform rcons read-tag <>)))

  (define function->line-xform
	(compose (tmap (lambda (line) (string-tokenize line)))
			 (tmap (match-lambda
					 ((name _ line . _)
					  (cons name (string->number line)))))))
  (define function->line
    (call-with-port (uctags "--output-format=xref" "--c-kinds=f"
							"--excmd=number" "-f" "-" module)
      (cut port-transduce function->line-xform rcons read-line <>)))

  (define function+return-type+parameters-xform
    (compose (tfilter (lambda (tag) (member (assoc-ref tag "scope") public-functions)))
             (tpartition (cut assoc-ref <> "scope"))
             (tmap (lambda (tag-group)
                     (let* ((scope (assoc-ref (car tag-group) "scope"))
							(return-type (assoc-ref functions+return-types scope))
							(line (assoc-ref function->line scope))
							(xform (tmap type-name)))
					   (cons* scope
							  (find-documentation line)
							  return-type
							  (list-transduce xform rcons tag-group)))))))
  (define function+return-type+parameters
    (call-with-port (uctags "--c-kinds=z" module)
      (cut port-transduce function+return-type+parameters-xform rcons read-tag <>)))
  (for-each
   (match-lambda
	 ((name docstring return-type . parameters)
	  (if (not (null? docstring))
		  (format #t "\n~a\n\n" (string-join docstring "\n")))
	  (format #t "~a ~a(~a);\n" return-type name
			  (string-join parameters ", "))))
   function+return-type+parameters))

(for-each generate-manual (cdr (program-arguments)))
