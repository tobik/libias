#!/bin/sh
set -eu

: ${AWK:=awk}
: ${MV:=mv}

for file in "$@"; do
	script="$(dirname "$0")/prototypes.scm \"${file}\""
	${AWK} -v script="${script}" '
$0 == "// Prototypes", $0 == "" {
	inside_marker = 1
	if ($0 == "// Prototypes") {
		print
	} else if ($0 == "") {
		system(script)
		inside_marker = 0
	}
}

!inside_marker {
	print
}
' "${file}" >"${file}.prototypes"
	${MV} "${file}.prototypes" "${file}"
done
