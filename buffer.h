// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#pragma once

libias_attr_returns_nonnull
struct LibiasBuffer *libias_buffer_new(void);

libias_attr_nonnull(1)
void libias_buffer_cleanup(struct LibiasBuffer **);

libias_attr_nonnull(1)
size_t libias_buffer_len(struct LibiasBuffer *);

libias_attr_nonnull(1, 2)
void libias_buffer_puts_buffer(struct LibiasBuffer *, struct LibiasBuffer *);

libias_attr_nonnull(1)
void libias_buffer_puts_size(struct LibiasBuffer *, size_t);

libias_attr_nonnull(1)
void libias_buffer_puts_ssize(struct LibiasBuffer *, ssize_t);

libias_attr_nonnull(1)
void libias_buffer_puts_int32(struct LibiasBuffer *, int32_t);

libias_attr_nonnull(1)
void libias_buffer_puts_int64(struct LibiasBuffer *, int64_t);

libias_attr_nonnull(1, 2)
void libias_buffer_puts_string(struct LibiasBuffer *, const char *);

#define libias_buffer_puts(file, data) \
	_Generic(\
		(data), \
		struct LibiasBuffer *: libias_buffer_puts_buffer, \
		char *: libias_buffer_puts_string, \
		const char *: libias_buffer_puts_string, \
		default: libias_macro_generic_integer_dispatch((data), libias_buffer_puts) \
		)((file), (data))

libias_attr_nonnull(1)
char *libias_buffer_data(struct LibiasBuffer *);

libias_attr_nonnull(1)
char *libias_buffer_data_copy(struct LibiasBuffer *);

libias_attr_nonnull(1)
char *libias_buffer_data_disown(struct LibiasBuffer *);

libias_attr_nonnull(1, 2)
void libias_buffer_write(struct LibiasBuffer *, const void *, size_t, size_t);

libias_attr_nonnull(1)
void libias_buffer_truncate(struct LibiasBuffer *);

libias_attr_nonnull(1)
void libias_buffer_truncate_at(struct LibiasBuffer *, size_t);

#define libias_scope_buffer(name) \
	libias_declare_variable_with_cleanup(struct LibiasBuffer, name, libias_buffer_cleanup, libias_buffer_new)
