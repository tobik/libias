// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#include <sys/param.h>
#include <stdlib.h>
#include <string.h>

#include "array.h"
#include "buffer.h"
#include "color.h"
#include "diff.h"
#include "diffutil.h"
#include "iterator.h"
#include "mempool.h"
#include "str.h"
#include "trait/compare.h"

struct Hunk {
	size_t start;
	size_t end;
};

// Prototypes
static libias_declare_compare(by_start_compare);
static struct LibiasArray *get_hunks(struct LibiasMempool *pool, struct LibiasDiff *p, size_t context);

// Constants
static struct LibiasCompareTrait *by_start = &(struct LibiasCompareTrait){
	.compare = by_start_compare,
	.context = NULL,
};

libias_define_compare(by_start_compare, struct Hunk, void)
{
	if (a->start < b->start) {
		return -1;
	} else if (a->start > b->start) {
		return 1;
	} else {
		return 0;
	}
}

struct LibiasArray *
get_hunks(struct LibiasMempool *pool, struct LibiasDiff *p, size_t context)
{
	libias_panic_unless(p->sessz > 0, "shortest common subsequence length is 0");
	if (context == 0) {
		struct LibiasArray *hunks = libias_mempool_array(pool);
		struct Hunk *h = libias_mempool_alloc(pool, struct Hunk);
		h->start = 0;
		h->end = p->sessz - 1;
		libias_array_append(hunks, h);
		return hunks;
	}

	struct LibiasArray *edit_ranges = libias_mempool_array(pool);
	struct Hunk *h = libias_mempool_alloc(pool, struct Hunk);
	bool first = true;
	bool last_common = true;
	for (size_t i = 0; i < p->sessz; i++) {
		switch (p->ses[i].type) {
		case LIBIAS_DIFF_COMMON:
			if (!last_common) {
				libias_array_append(edit_ranges, h);
				h = libias_mempool_alloc(pool, struct Hunk);
			}
			last_common = true;
			first = true;
			break;
		case LIBIAS_DIFF_ADD:
		case LIBIAS_DIFF_DELETE:
			last_common = false;
			if (first) {
				h->start = i;
				first = false;
			}
			h->end = i;
			break;
		}
	}
	if (!first) {
		libias_array_append(edit_ranges, h);
	}

	libias_array_foreach(edit_ranges, struct Hunk *, h) {
		if (h->start > context) {
			h->start -= context;
		}
		h->end = MIN(h->end + context, p->sessz - 1);
	}
	libias_array_sort(edit_ranges, by_start);

	struct LibiasArray *hunks = libias_mempool_array(pool);
	struct Hunk *last = NULL;
	libias_array_foreach(edit_ranges, struct Hunk *, h) {
		if (!last) {
			last = h;
			continue;
		} else {
			if (h->start <= last->end) {
				if (h->end > last->end) {
					last->end = h->end;
				}
			} else if (h->start > last->end) {
				libias_array_append(hunks, last);
				last = h;
			}
		}
	}
	if (last) {
		libias_array_append(hunks, last);
	}

	libias_panic_unless(libias_array_len(hunks) > 0, "no hunks found");
	return hunks;
}

void
libias_diff_to_patch(
	struct LibiasDiff *p,
	struct LibiasBuffer *result,
	size_t context,
	bool color,
	LibiasDiffToStringFn tostring,
	void *tostring_userdata)
{
	if (p->sessz == 0) {
		return;
	}

	libias_scope_mempool(pool);
	const char *color_add = LIBIAS_ANSI_COLOR_GREEN;
	const char *color_context = LIBIAS_ANSI_COLOR_CYAN;
	const char *color_delete = LIBIAS_ANSI_COLOR_RED;
	const char *color_reset = LIBIAS_ANSI_COLOR_RESET;
	if (!color) {
		color_add = color_context = color_delete = color_reset = "";
	}

	struct LibiasArray *hunks = get_hunks(pool, p, context);
	libias_array_foreach(hunks, struct Hunk *, h) {
		size_t origin_len = 0;
		size_t target_len = 0;
		for (size_t i = h->start; i <= h->end; i++) {
			switch (p->ses[i].type) {
			case LIBIAS_DIFF_ADD:
				target_len++;
				break;
			case LIBIAS_DIFF_COMMON:
				origin_len++;
				target_len++;
				break;
			case LIBIAS_DIFF_DELETE:
				origin_len++;
				break;
			}
		}
		size_t origin_start = p->ses[h->start].originIdx;
		if (origin_start == 0) {
			origin_start = 1;
		}
		size_t target_start = p->ses[h->start].targetIdx;
		if (origin_len > 1) {
			libias_buffer_puts(result, color_context);
			libias_buffer_puts(result, "@@ -");
			libias_buffer_puts(result, origin_start);
			libias_buffer_puts(result, ",");
			libias_buffer_puts(result, origin_len);
		} else {
			libias_buffer_puts(result, color_context);
			libias_buffer_puts(result, "@@ -");
			libias_buffer_puts(result, origin_start);
		}
		if (target_len > 1) {
			libias_buffer_puts(result, " +");
			libias_buffer_puts(result, target_start);
			libias_buffer_puts(result, ",");
			libias_buffer_puts(result, target_len);
			libias_buffer_puts(result, " @@");
			libias_buffer_puts(result, color_reset);
			libias_buffer_puts(result, "\n");
		} else {
			libias_buffer_puts(result, " +");
			libias_buffer_puts(result, target_start);
			libias_buffer_puts(result, " @@");
			libias_buffer_puts(result, color_reset);
			libias_buffer_puts(result, "\n");
		}
		for (size_t i = h->start; i <= h->end; i++) {
			char *line;
			if (tostring) {
				line = libias_mempool_take(pool, tostring(*(void **)p->ses[i].e, tostring_userdata));
			} else {
				line = *(void **)p->ses[i].e;
			}
			switch (p->ses[i].type) {
			case LIBIAS_DIFF_ADD:
				libias_buffer_puts(result, color_add);
				libias_buffer_puts(result, "+");
				libias_buffer_puts(result, line);
				libias_buffer_puts(result, color_reset);
				libias_buffer_puts(result, "\n");
				break;
			case LIBIAS_DIFF_COMMON:
				libias_buffer_puts(result, " ");
				libias_buffer_puts(result, line);
				libias_buffer_puts(result, "\n");
				break;
			case LIBIAS_DIFF_DELETE:
				libias_buffer_puts(result, color_delete);
				libias_buffer_puts(result, "-");
				libias_buffer_puts(result, line);
				libias_buffer_puts(result, color_reset);
				libias_buffer_puts(result, "\n");
				break;
			}
		}
	}
}
