// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#pragma once

#define libias_test_ok()

#define libias_test_fail_loc(file, line, col, msg) \
do { \
	if (failureslen < nitems(failures)) { \
		char *buf = libias_str_printf("%s:%d:%d: FAIL: %s", file, line, col, msg); \
		libias_mempool_take(test_pool__, buf); \
		failures[failureslen++] = buf; \
	} \
} while(0)

#define libias_test_fail(msg) libias_test_fail_loc(__FILE__, __LINE__, 1, msg)

#define libias_test(x) \
do { \
	if ((x)) { \
		libias_test_ok(); \
	} else { \
		libias_test_fail(#x); \
	} \
} while(0)

#define libias_test_if(x) \
if (!(x)) \
	libias_test_fail(#x)

#define libias_test_str(cmp, a, b) \
do { \
	const char *s1 = (a); \
	const char *s2 = (b); \
	if (s1 && s2 && strcmp(s1, s2) cmp 0) { \
		libias_test_ok(); \
	} else { \
		libias_test_fail(""#a" "#cmp" "#b""); \
	} \
} while(0)

#define libias_test_streq(a, b) libias_test_str(==, a, b)
#define libias_test_strneq(a, b) libias_test_str(!=, a, b)

#define libias_tests() \
static char *failures[512]; \
static size_t failureslen; \
static void run_tests(struct LibiasMempool *, struct LibiasMempool *); \
int main(int argc, char *argv[]) { \
	libias_scope_mempool(pool); \
	run_tests(pool, pool); \
	for (size_t i = 0; i < failureslen; i++) { \
		puts(failures[i]); \
	} \
	return failureslen != 0; \
} \
void run_tests(struct LibiasMempool *pool, struct LibiasMempool *test_pool__)
