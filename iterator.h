// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#pragma once

typedef bool (*LibiasIteratorNextFn)(struct LibiasIterator **, size_t *, void **, void **);
typedef void (*LibiasIteratorContextCleanupFn)(void **);

libias_attr_nonnull(1, 2, 3)
libias_attr_returns_nonnull
struct LibiasIterator *libias_iterator_new(LibiasIteratorNextFn, void *, LibiasIteratorContextCleanupFn);

libias_attr_nonnull(1)
void libias_iterator_cleanup(struct LibiasIterator **);

libias_attr_nonnull(1, 2, 3, 4)
bool libias_iterator_next(struct LibiasIterator **, size_t *, void **, void **);

libias_attr_nonnull(1)
libias_attr_returns_nonnull
void *libias_iterator_context(struct LibiasIterator *);

libias_attr_nonnull(1)
libias_attr_returns_nonnull
struct LibiasMempool *libias_iterator_pool(struct LibiasIterator *);

#define libias_iterator_foreach(...) \
	libias_iterator_foreach__(libias_macro_gensym(iter_var), libias_macro_gensym(iter_state), __VA_ARGS__)
#define libias_iterator_foreach__(ITERVAR, ITERSTATEVAR, constructor, KEYTYPE, KEYVAR, VALTYPE, VALVAR) \
	for (struct LibiasIterator *ITERVAR libias_attr_cleanup(libias_iterator_cleanup) = constructor; ITERVAR != NULL; libias_cleanup(&ITERVAR)) \
	for (size_t KEYVAR##_index = 0; ITERVAR != NULL; libias_cleanup(&ITERVAR)) \
	for (KEYTYPE KEYVAR = NULL; ITERVAR != NULL; libias_cleanup(&ITERVAR)) \
	for (VALTYPE VALVAR = NULL; ITERVAR != NULL; libias_cleanup(&ITERVAR)) \
	for (bool ITERSTATEVAR = libias_iterator_next(&ITERVAR, &KEYVAR##_index, (void **)&KEYVAR, (void **)&VALVAR); ITERSTATEVAR; ITERSTATEVAR = libias_iterator_next(&ITERVAR, &KEYVAR##_index, (void **)&KEYVAR, (void **)&VALVAR))
