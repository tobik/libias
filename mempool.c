// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#include <stdlib.h>
#include <unistd.h>

#include "hashmap.h"
#include "iterator.h"
#include "mempool.h"
#include "trait/compare.h"
#include "trait/hash.h"

struct LibiasMempool {
	struct LibiasHashmap *map;
};

struct LibiasMempool *
libias_mempool_new(void)
{
	struct LibiasMempool *pool = libias_alloc(struct LibiasMempool);
	pool->map = libias_hashmap_new(libias_compare_ptr, libias_hash_ptr);
	return pool;
}

void
libias_mempool_cleanup(struct LibiasMempool **pool_ptr)
{
	struct LibiasMempool *pool = *pool_ptr;
	if (!pool) {
		return;
	}
	libias_mempool_release_all(pool);
	libias_cleanup(&pool->map);
	libias_free(pool);
	*pool_ptr = NULL;
}

void *
libias_mempool_add__(
	struct LibiasMempool *pool,
	void *ptr,
	void *cleanup)
{
	if (!pool) {
		return ptr;
	}

	libias_hashmap_replace(pool->map, ptr, cleanup);

	return ptr;
}

void
libias_mempool_forget(struct LibiasMempool *pool, void *ptr)
{
	if (!pool) {
		return;
	}

	libias_hashmap_remove(pool->map, ptr);
}

void
libias_mempool_inherit(struct LibiasMempool *pool, struct LibiasMempool *other)
{
	libias_hashmap_foreach(other->map, void *, ptr, void *, cleanup) {
		libias_hashmap_replace(pool->map, ptr, cleanup);
	}
}

void
libias_mempool_move(struct LibiasMempool *pool, void *ptr, struct LibiasMempool *other)
{
	if (!pool || pool == other) {
		return;
	}

	void *cleanup = libias_hashmap_get(pool->map, ptr);
	if (cleanup) {
		libias_hashmap_remove(pool->map, ptr);
		libias_hashmap_replace(other->map, ptr, cleanup);
	}
}

void
libias_mempool_release(struct LibiasMempool *pool, void *ptr)
{
	if (!pool) {
		return;
	}

	void (*cleanup)(void **) = libias_hashmap_get(pool->map, ptr);
	if (cleanup) {
		libias_hashmap_remove(pool->map, ptr);
		cleanup(&ptr);
	}
}

void
libias_mempool_release_all(struct LibiasMempool *pool)
{
	if (!pool) {
		return;
	}

	libias_hashmap_foreach(pool->map, void *, ptr, void *, cleanup_) {
		void (*cleanup)(void **) = cleanup_;
		cleanup(&ptr);
	}

	libias_hashmap_truncate(pool->map);
}

char *
libias_mempool_alloc_buffer(
	struct LibiasMempool *pool,
	size_t sz)
{
	return libias_mempool_add(pool, libias_alloc_buffer(sz), libias_runtime_cleanup);
}
