// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#pragma once

libias_attr_nonnull(1)
libias_attr_returns_nonnull
struct LibiasMap *libias_map_new(struct LibiasCompareTrait *);

libias_attr_nonnull(1)
void libias_map_cleanup(struct LibiasMap **);

libias_attr_nonnull(1, 2, 3)
void libias_map_add(struct LibiasMap *, const void *, const void *);

libias_attr_nonnull(1, 2, 3)
void libias_map_replace(struct LibiasMap *, const void *, const void *);

libias_attr_nonnull(1, 2)
void libias_map_remove(struct LibiasMap *, const void *);

libias_attr_nonnull(1, 2)
void *libias_map_get(struct LibiasMap *, const void *);

libias_attr_nonnull(1, 2)
bool libias_map_contains_p(struct LibiasMap *, const void *);

libias_attr_nonnull(1)
size_t libias_map_len(struct LibiasMap *);

libias_attr_nonnull(1)
void libias_map_truncate(struct LibiasMap *);

libias_attr_nonnull(1)
libias_attr_returns_nonnull
struct LibiasArray *libias_map_keys(struct LibiasMap *);

libias_attr_nonnull(1)
libias_attr_returns_nonnull
struct LibiasArray *libias_map_values(struct LibiasMap *);

libias_attr_nonnull(1)
libias_attr_returns_nonnull
struct LibiasIterator *libias_map_iterator(struct LibiasMap *, ssize_t, ssize_t);

#define libias_map_foreach_slice(MAP, A, B, KEYTYPE, KEYVAR, VALTYPE, VALVAR) \
	libias_iterator_foreach(libias_map_iterator(MAP, A, B), KEYTYPE, KEYVAR, VALTYPE, VALVAR)
#define libias_map_foreach(MAP, ...) \
	libias_map_foreach_slice(MAP, 0, -1, __VA_ARGS__)

#define libias_scope_map(name, cmp) \
	libias_declare_variable_with_cleanup(struct LibiasMap, name, libias_map_cleanup, libias_map_new, cmp)
