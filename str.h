// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#pragma once

libias_attr_nonnull(1, 2)
libias_attr_returns_nonnull
char *libias_str_common_prefix(const char *, const char *);

libias_attr_nonnull(1)
libias_attr_returns_nonnull
char *libias_str_dup(const char *);

libias_attr_nonnull(1)
libias_attr_returns_nonnull
char *libias_str_ndup(const char *, const size_t);

libias_attr_nonnull(1, 2)
bool libias_str_suffix_p(const char *, const char *);

libias_attr_nonnull(1, 2)
bool libias_str_prefix_p(const char *, const char *);

libias_attr_nonnull(1, 2)
char *libias_str_join(struct LibiasArray *, const char *);

libias_attr_nonnull(1, 3)
libias_attr_returns_nonnull
char *str_map(const char *, size_t, int (*)(int));

libias_attr_nonnull(1)
libias_attr_printflike(1, 2)
libias_attr_returns_nonnull
char *libias_str_printf(const char *, ...);

libias_attr_nonnull(1)
libias_attr_returns_nonnull
char *libias_str_repeat(const char *, const size_t);

libias_attr_nonnull(1)
libias_attr_returns_nonnull
char *libias_str_slice(const char *, const ssize_t, const ssize_t);

libias_attr_nonnull(1, 2, 3)
libias_attr_returns_nonnull
struct LibiasArray *libias_str_split(struct LibiasMempool *, const char *, const char *);

libias_attr_nonnull(1, 2, 4)
libias_attr_returns_nonnull
struct LibiasArray *libias_str_nsplit(struct LibiasMempool *, const char *, const size_t, const char *);

libias_attr_nonnull(1)
libias_attr_returns_nonnull
char *libias_str_trim(const char *);

libias_attr_nonnull(1)
libias_attr_returns_nonnull
char *libias_str_triml(const char *);

libias_attr_nonnull(1)
libias_attr_returns_nonnull
char *libias_str_trimr(const char *);

libias_attr_nonnull(1)
libias_attr_returns_nonnull
char *libias_str_trim_ws(const char *, char **, char **);

libias_attr_nonnull(1)
libias_attr_returns_nonnull
char *libias_str_triml_ws(const char *, char **);

libias_attr_nonnull(1)
libias_attr_returns_nonnull
char *libias_str_trimr_ws(const char *, char **);

extern struct LibiasCompareTrait *libias_str_casecompare;
extern struct LibiasCompareTrait *libias_str_compare;
