// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#include <sys/param.h>
#include <sys/stat.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "buffer.h"
#include "io.h"
#include "iterator.h"
#include "str.h"
#include "util.h"

struct LibiasDirectory {
	DIR *dir;
};

struct LibiasDirectoryIterator {
	DIR *dir;
	bool forwarded;
	size_t i;
	size_t len;
};

struct LibiasFile {
	int fd;
};

struct LibiasFileLineIterator {
	FILE *stream;
	char *line;
	size_t linecap;
	ssize_t linelen;

	bool forwarded;
	size_t i;
	size_t len;
};

// Prototypes
static void libias_directory_iterator_cleanup(void **iter_ptr);
static bool libias_directory_iterator_next(struct LibiasIterator **it_ptr, size_t *index, void **key, void **val);
static void libias_file_line_iterator_cleanup(void **iter_ptr);
static bool libias_file_line_iterator_next(struct LibiasIterator **it_ptr, size_t *index, void **line, void **linelen);
static ssize_t libias_file_write(struct LibiasFile *file, const void *buf, size_t size, size_t nmemb);

// Constants
static const size_t READ_BUF_SIZE = 4096;
static const size_t WRITE_BUF_SIZE = 4096;
static const size_t SLURP_BUF_SIZE = 4096;

struct LibiasDirectory *
libias_directory_open(const char *path)
{
	return libias_directory_openat(NULL, path);
}

struct LibiasDirectory *
libias_directory_openat(
	struct LibiasDirectory *root,
	const char *path)
{
	int dirfd;
	if (root) {
		dirfd = libias_directory_fd(root);
	} else {
		dirfd = AT_FDCWD;
	}

	int fd = openat(dirfd, path, O_DIRECTORY);
	if (fd == -1) {
		return NULL;
	} else {
		DIR *dir = fdopendir(fd);
		if (dir == NULL) {
			close(fd);
			return NULL;
		}
		struct LibiasDirectory *directory = libias_alloc(struct LibiasDirectory);
		directory->dir = dir;
		return directory;
	}
}

void
libias_directory_cleanup(struct LibiasDirectory **directory_ptr)
{
	struct LibiasDirectory *directory = *directory_ptr;
	if (directory) {
		closedir(directory->dir);
		libias_free(directory);
		*directory_ptr = NULL;
	}
}

int
libias_directory_chdir(struct LibiasDirectory *dir)
{
	return fchdir(dirfd(dir->dir));
}

int
libias_directory_fd(struct LibiasDirectory *dir)
{
	return dirfd(dir->dir);
}

int
libias_directory_stat(struct LibiasDirectory *dir, struct stat *statbuf)
{
	return fstat(dirfd(dir->dir), statbuf);
}

struct LibiasFile *
libias_file_open(
	const char *path,
	int flags,
	mode_t mode)
{
	return libias_file_openat(NULL, path, flags, mode);
}

struct LibiasFile *
libias_file_openat(
	struct LibiasDirectory *root,
	const char *path,
	int flags,
	mode_t mode)
{
	int dirfd;
	if (root) {
		dirfd = libias_directory_fd(root);
	} else {
		dirfd = AT_FDCWD;
	}
	int fd = openat(dirfd, path, flags, mode);
	if (fd == -1) {
		return NULL;
	} else {
		struct LibiasFile *file = libias_alloc(struct LibiasFile);
		file->fd = fd;
		return file;
	}
}

struct LibiasFile *
libias_file_openfd(int fd)
{
	struct LibiasFile *file = libias_alloc(struct LibiasFile);
	file->fd = fd;
	return file;
}

void
libias_file_cleanup(struct LibiasFile **file_ptr)
{
	struct LibiasFile *file = *file_ptr;
	if (file) {
		close(file->fd);
		libias_free(file);
		*file_ptr = NULL;
	}
}

ssize_t
libias_file_read(
	struct LibiasFile *file,
	struct LibiasBuffer *buffer,
	size_t size,
	size_t nmemb)
{
	size_t left;
	if (ckd_mul(&left, size, nmemb)) {
		libias_panic("integer overflow: %zu * %zu", size, nmemb);
	}

	char buf[READ_BUF_SIZE];
	ssize_t bytes = 0;
	size_t pos = 0;
	while (left > 0 && (bytes = read(file->fd, buf, left)) != 0) {
		if (bytes < 0) {
			if (errno == EAGAIN || errno == EWOULDBLOCK) {
				continue;
			}
			return bytes;
		}
		pos += bytes;
		left -= bytes;
		libias_buffer_write(buffer, buf, bytes, 1);
	}

	return pos;
}

ssize_t
libias_file_write(
	struct LibiasFile *file,
	const void *buf,
	size_t size,
	size_t nmemb)
{
	size_t left;
	if (ckd_mul(&left, size, nmemb)) {
		libias_panic("integer overflow: %zu * %zu", size, nmemb);
	}

	size_t bufsize = MIN(left, WRITE_BUF_SIZE);
	ssize_t bytes = 0;
	size_t pos = 0;
	while (left > 0 && (bytes = write(file->fd, buf + pos, bufsize)) != 0) {
		if (bytes < 0) {
			if (errno == EAGAIN || errno == EWOULDBLOCK) {
				continue;
			}
			return bytes;
		}
		pos += bytes;
		left -= bytes;
		bufsize = MIN(left, WRITE_BUF_SIZE);
	}

	return pos;
}

int
libias_file_printf(struct LibiasFile *file, const char *format, ...)
{
	va_list ap;
	va_start(ap, format);
	int result = vdprintf(file->fd, format, ap);
	va_end(ap);
	return result;
}

ssize_t
libias_file_put_buffer(
	struct LibiasFile *file,
	struct LibiasBuffer *buffer)
{
	size_t len = libias_buffer_len(buffer);
	char *buf = libias_buffer_data(buffer);
	return libias_file_write(file, buf, len, 1);
}

ssize_t
libias_file_put_string(
	struct LibiasFile *file,
	const char *s)
{
	size_t len = strlen(s);
	return libias_file_write(file, s, len, 1);
}

int
libias_file_fd(struct LibiasFile *file)
{
	return file->fd;
}

off_t
libias_file_len(struct LibiasFile *file)
{
	struct stat statbuf;
	if (libias_file_stat(file, &statbuf) == -1) {
		return (off_t)-1;
	}
	return statbuf.st_size;
}

off_t
libias_file_truncate(struct LibiasFile *file)
{
	return libias_file_truncate_at(file, 0);
}

off_t
libias_file_truncate_at(
	struct LibiasFile *file,
	off_t length)
{
	return ftruncate(file->fd, length);
}

off_t
libias_file_rewind(
	struct LibiasFile *file,
	off_t offset)
{
	return lseek(file->fd, offset, SEEK_CUR);
}

off_t
libias_file_set_position(
	struct LibiasFile *file,
	off_t offset)
{
	return lseek(file->fd, offset, SEEK_SET);
}

int
libias_file_stat(struct LibiasFile *file, struct stat *statbuf)
{
	return fstat(file->fd, statbuf);
}

bool
libias_file_can_use_colors(struct LibiasFile *file)
{
	if (getenv("CLICOLOR_FORCE") != NULL) {
		return true;
	}

	if (getenv("NO_COLOR") != NULL) {
		return false;
	}

	if (isatty(file->fd) == 0) {
		return false;
	}

	return true;
}

struct LibiasIterator *
libias_file_line_iterator(struct LibiasFile *file, ssize_t a, ssize_t b)
{
	struct LibiasFileLineIterator *iter = libias_alloc(struct LibiasFileLineIterator);
	libias_slice_to_range(SSIZE_MAX, a, b, &iter->i, &iter->len);
	iter->stream = fdopen(dup(file->fd), "r");
	return libias_iterator_new(
		libias_file_line_iterator_next,
		iter,
		libias_file_line_iterator_cleanup);
}

void
libias_file_line_iterator_cleanup(void **iter_ptr)
{
	struct LibiasFileLineIterator *iter = *iter_ptr;
	if (iter) {
		if (iter->stream) {
			fclose(iter->stream);
		}
		libias_free(iter->line);
		libias_free(iter);
		*iter_ptr = NULL;
	}
}

bool
libias_file_line_iterator_next(
	struct LibiasIterator **it_ptr,
	size_t *index,
	void **line,
	void **linelen)
{
	struct LibiasFileLineIterator *iter = libias_iterator_context(*it_ptr);

	if (!iter->stream) {
		return false;
	}

	if (!iter->forwarded) {
		iter->forwarded = true;
		for (size_t i = 0; i < iter->i; i++) {
			iter->linelen = getline(
				&iter->line,
				&iter->linecap,
				iter->stream);
			if (iter->linelen == 0) {
				return false;
			}
		}
	}

	iter->linelen = getline(
		&iter->line,
		&iter->linecap,
		iter->stream);
	if (iter->linelen > 0 && iter->i < iter->len) {
		if (iter->line[iter->linelen - 1] == '\n') {
			iter->line[iter->linelen - 1] = 0;
			iter->linelen--;
		}
		*index = iter->i++;
		*line = iter->line;
		*linelen = &iter->linelen;
		return true;
	}

	return false;
}

struct LibiasBuffer *
libias_file_slurp(struct LibiasFile *file)
{
	auto buf = libias_buffer_new();
	ssize_t bytes;
	while ((bytes = libias_file_read(file, buf, SLURP_BUF_SIZE, 1)) != 0) {
		if (bytes < 0) {
			if (errno == EAGAIN || errno == EWOULDBLOCK) {
				continue;
			}
			libias_cleanup(&buf);
			return NULL;
		}
	}
	return buf;
}

char *
libias_symlink_read(struct LibiasDirectory *root, const char *path)
{
	int dirfd = AT_FDCWD;
	if (root) {
		dirfd = libias_directory_fd(root);
	}
	char buf[PATH_MAX];
	ssize_t len = readlinkat(dirfd, path, buf, sizeof(buf));
	if (len != -1) {
		return libias_str_ndup(buf, len);
	}
	return NULL;
}

// Update the symlink TO to point to FROM. TO is rooted in ROOT. If PREV
// is non-NULL then the old value ot the symlink is returned or NULL if
// the symlink didn't exist yet. The caller must take care to free PREV.
bool
libias_symlink_update(
	struct LibiasDirectory *root,
	const char *from,
	const char *to,
	char **prev)
{
	int dirfd = AT_FDCWD;
	if (root) {
		dirfd = libias_directory_fd(root);
	}

	if (prev) {
		*prev = NULL;
	}
	while (symlinkat(from, dirfd, to) == -1) {
		if (errno == EEXIST) {
			if (prev) {
				*prev = libias_symlink_read(root, to);
			}
			if (unlinkat(dirfd, to, 0) == -1) {
				if (prev) {
					libias_free(*prev);
					*prev = NULL;
				}
				return false;
			}
		} else {
			if (prev) {
				*prev = NULL;
			}
			return false;
		}
	}

	return true;
}

struct LibiasIterator *
libias_directory_iterator(struct LibiasDirectory *dir, ssize_t a, ssize_t b)
{
	struct LibiasDirectoryIterator *iter = libias_alloc(struct LibiasDirectoryIterator);
	libias_slice_to_range(SSIZE_MAX, a, b, &iter->i, &iter->len);
	iter->dir = dir->dir;
	return libias_iterator_new(
		libias_directory_iterator_next,
		iter,
		libias_directory_iterator_cleanup);
}

void
libias_directory_iterator_cleanup(void **iter_ptr)
{
	struct LibiasDirectoryIterator *iter = *iter_ptr;
	if (iter) {
		libias_free(iter);
		*iter_ptr = NULL;
	}
}

bool
libias_directory_iterator_next(
	struct LibiasIterator **it_ptr,
	size_t *index,
	void **key,
	void **val)
{
	struct LibiasDirectoryIterator *iter = libias_iterator_context(*it_ptr);

	if (!iter->forwarded) {
		iter->forwarded = true;
		for (size_t i = 0; i < iter->i; i++) {
			if (!readdir(iter->dir)) {
				return false;
			}
		}
	}

	if (iter->i < iter->len) {
		struct dirent *ent = readdir(iter->dir);
		if (!ent) {
			return false;
		}
		*index = iter->i++;
		*key = ent;
		*val = ent;
		return true;
	}

	return false;
}
