// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#include <stdlib.h>

#include "array.h"
#include "hashmap.h"
#include "iterator.h"
#include "trait/compare.h"
#include "trait/hash.h"
#include "util.h"

#define uthash_malloc(sz) \
	libias_alloc_buffer(sz)
#define uthash_free(ptr,sz) \
	libias_free(ptr)
#define HASH_FUNCTION(keyptr,keylen,hashv) \
	hashv = map->hash.hash(keyptr, keylen, map->hash.context)
#define HASH_KEYCMP(a,b,n) \
	libias_hashmap_keycmp(map, (a), (b))
#include "uthash.h"

struct LibiasHashmapEntry {
	const void *key;
	const void *value;
	UT_hash_handle hh;
};

struct LibiasHashmap {
	struct LibiasCompareTrait compare;
	struct LibiasHashTrait hash;
	struct LibiasHashmapEntry *entries;
};

struct LibiasHashmapIterator {
	struct LibiasHashmapEntry *current;
	size_t index;
	size_t len;
	size_t start;
};

// Prototypes
static void libias_hashmap_add_entry(struct LibiasHashmap *map, const void *key, const void *value);
static struct LibiasHashmapEntry *libias_hashmap_get_entry(struct LibiasHashmap *map, const void *key);
static void libias_hashmap_iterator_cleanup(void **iter_ptr);
static bool libias_hashmap_iterator_next(struct LibiasIterator **it_ptr, size_t *index, void **key, void **value);
static int libias_hashmap_keycmp(struct LibiasHashmap *map, const void *a, const void *b);

struct LibiasHashmap *
libias_hashmap_new(
	struct LibiasCompareTrait *compare,
	struct LibiasHashTrait *hash)
{
	struct LibiasHashmap *map = libias_alloc(struct LibiasHashmap);
	map->hash = *hash;
	map->compare = *compare;
	return map;
}

void
libias_hashmap_cleanup(struct LibiasHashmap **map_ptr)
{
	struct LibiasHashmap *map = *map_ptr;
	if (map) {
		libias_hashmap_truncate(map);
		libias_free(map);
		*map_ptr = NULL;
	}
}

int
libias_hashmap_keycmp(
	struct LibiasHashmap *map,
	const void *a,
	const void *b)
{
	return map->compare.compare(a, b, map->compare.context);
}

struct LibiasHashmapEntry *
libias_hashmap_get_entry(
	struct LibiasHashmap *map,
	const void *key)
{
	struct LibiasHashmapEntry *entry = NULL;
	size_t keylen = map->hash.datalen(&key, map->hash.context);
	HASH_FIND(hh, map->entries, &key, keylen, entry);
	return entry;
}

void
libias_hashmap_add_entry(
	struct LibiasHashmap *map,
	const void *key,
	const void *value)
{
	struct LibiasHashmapEntry *entry = libias_alloc(struct LibiasHashmapEntry);
	entry->key = key;
	entry->value = value;
	size_t keylen = map->hash.datalen(&key, map->hash.context);
	HASH_ADD(hh, map->entries, key, keylen, entry);
}

void
libias_hashmap_add(
	struct LibiasHashmap *map,
	const void *key,
	const void *value)
{
	struct LibiasHashmapEntry *entry = libias_hashmap_get_entry(map, key);
	if (!entry) {
		libias_hashmap_add_entry(map, key, value);
	}
}

void
libias_hashmap_replace(
	struct LibiasHashmap *map,
	const void *key,
	const void *value)
{
	struct LibiasHashmapEntry *entry = libias_hashmap_get_entry(map, key);
	if (entry) {
		entry->value = value;
	} else {
		libias_hashmap_add_entry(map, key, value);
	}
}

void
libias_hashmap_remove(
	struct LibiasHashmap *map,
	const void *key)
{
	struct LibiasHashmapEntry *entry = libias_hashmap_get_entry(map, key);
	if (entry) {
		HASH_DEL(map->entries, entry);
		libias_free(entry);
	}
}

void *
libias_hashmap_get(
	struct LibiasHashmap *map,
	const void *key)
{
	struct LibiasHashmapEntry *entry = libias_hashmap_get_entry(map, key);
	if (entry) {
		return (void *)entry->value;
	} else {
		return NULL;
	}
}

bool
libias_hashmap_contains_p(
	struct LibiasHashmap *map,
	const void *key)
{
	return libias_hashmap_get_entry(map, key) != NULL;
}

size_t
libias_hashmap_len(struct LibiasHashmap *map)
{
	return HASH_COUNT(map->entries);
}

void
libias_hashmap_truncate(struct LibiasHashmap *map)
{
	struct LibiasHashmapEntry *current, *tmp;
	HASH_ITER(hh, map->entries, current, tmp) {
		HASH_DEL(map->entries, current);
		libias_free(current);
	}
}

struct LibiasArray *
libias_hashmap_keys(struct LibiasHashmap *map)
{
	struct LibiasArray *array = libias_array_new();
	for (struct LibiasHashmapEntry *entry = map->entries;
		 entry != NULL;
		 entry = entry->hh.next)
	{
		libias_array_append(array, entry->key);
	}
	return array;
}

struct LibiasArray *
libias_hashmap_values(struct LibiasHashmap *map)
{
	struct LibiasArray *array = libias_array_new();
	for (struct LibiasHashmapEntry *entry = map->entries;
		 entry != NULL;
		 entry = entry->hh.next)
	{
		libias_array_append(array, entry->value);
	}
	return array;
}

struct LibiasIterator *
libias_hashmap_iterator(
	struct LibiasHashmap *map,
	ssize_t a,
	ssize_t b)
{
	struct LibiasHashmapIterator *iter = libias_alloc(struct LibiasHashmapIterator);
	libias_slice_to_range(HASH_COUNT(map->entries), a, b, &iter->start, &iter->len);
	iter->current = map->entries;
	return libias_iterator_new(
		libias_hashmap_iterator_next,
		iter,
		libias_hashmap_iterator_cleanup);
}

void
libias_hashmap_iterator_cleanup(void **iter_ptr)
{
	struct LibiasHashmapIterator *iter = *iter_ptr;
	if (iter) {
		libias_free(iter);
		*iter_ptr = NULL;
	}
}

bool
libias_hashmap_iterator_next(
	struct LibiasIterator **it_ptr,
	size_t *index,
	void **key,
	void **value)
{
	struct LibiasHashmapIterator *iter = libias_iterator_context(*it_ptr);

	if (iter->start > 0) {
		for (iter->index = 0; iter->current && iter->index < iter->start; iter->index++) {
			iter->current = iter->current->hh.next;
		}
		iter->start = 0;
	}

	if (!iter->current) {
		return false;
	}

	*index = iter->index++;
	*key = (void *)iter->current->key;
	*value = (void *)iter->current->value;
	iter->current = iter->current->hh.next;

	return true;
}
