// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "array.h"
#include "buffer.h"
#include "diff.h"
#include "diffutil.h"
#include "io.h"
#include "mempool.h"
#include "str.h"
#include "test.h"

libias_tests() {
	struct LibiasArray *a = libias_mempool_array(pool);
	for (size_t i = 0; i < 16; i++) {
		libias_array_append(a, "1");
	}
	struct LibiasArray *b = libias_mempool_array(pool);
	libias_array_append(b, "2");
	libias_array_append(b, "2");
	for (size_t i = 0; i < 8; i++) {
		libias_array_append(b, "1");
	}
	libias_array_append(b, "3");
	for (size_t i = 0; i < 7; i++) {
		libias_array_append(b, "1");
	}

	struct LibiasDiff *d;
	libias_test_if(d = libias_array_diff(a, b, libias_str_compare));
	{
		libias_scope_buffer(actual_buf);
		libias_diff_to_patch(d, actual_buf, 3, 0, NULL, NULL);
		char *actual = libias_buffer_data(actual_buf);
		struct LibiasFile *f = libias_file_open("tests/diff/0001.diff", O_RDONLY, 0);
		libias_test(f);
		auto expected = libias_file_slurp(f);
		libias_test_streq(actual, libias_buffer_data(expected));
		libias_cleanup(&expected);
		libias_cleanup(&f);
		libias_cleanup(&d);
	}
}
