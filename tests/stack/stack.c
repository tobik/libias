// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "iterator.h"
#include "mempool.h"
#include "stack.h"
#include "str.h"
#include "test.h"

libias_tests() {
	libias_scope_stack(stack);
	libias_stack_push(stack, "1");
	libias_stack_push(stack, "2");
	libias_stack_push(stack, "3");
	libias_test(libias_stack_len(stack) == 3);
	libias_test_streq(libias_stack_peek(stack), "3");
	libias_test_streq(libias_stack_pop(stack), "3");
	libias_test(libias_stack_len(stack) == 2);
	libias_test_streq(libias_stack_pop(stack), "2");
	libias_test(libias_stack_len(stack) == 1);
	libias_test_streq(libias_stack_pop(stack), "1");
	libias_test(libias_stack_len(stack) == 0);
	libias_test(libias_stack_pop(stack) == NULL);

	libias_stack_push(stack, "1");
	libias_stack_push(stack, "2");
	libias_stack_push(stack, "3");
	size_t i = 0;
	libias_stack_foreach(stack, const char *, s) {
		switch(s_index) {
		case 0: libias_test_streq(s, "3"); break;
		case 1: libias_test_streq(s, "2"); break;
		case 2: libias_test_streq(s, "1"); break;
		default: libias_test(0); break;
		}
		i++;
	}
	libias_test(i == 3);
}
