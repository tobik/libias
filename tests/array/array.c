// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "array.h"
#include "iterator.h"
#include "mempool.h"
#include "str.h"
#include "test.h"

libias_tests() {
	libias_scope_array(array);
	libias_array_append(array, "1");
	libias_array_append(array, "2");
	libias_array_append(array, "3");
	size_t i = 0;
	libias_array_foreach(array, char *, s) {
		libias_test(i == s_index);
		switch (i++) {
		case 0: libias_test_streq(s, "1"); break;
		case 1: libias_test_streq(s, "2"); break;
		case 2: libias_test_streq(s, "3"); break;
		}
	}
	libias_test(i == 3);

	libias_test_streq(libias_array_remove(array, 1), "2");
	i = 0;
	libias_array_foreach(array, char *, s) {
		libias_test(i == s_index);
		switch (i++) {
		case 0: libias_test_streq(s, "1"); break;
		case 1: libias_test_streq(s, "3"); break;
		}
	}
	libias_test(i == 2);

	libias_test_streq(libias_array_remove(array, 0), "1");
	i = 0;
	libias_array_foreach(array, char *, s) {
		libias_test(i == s_index);
		switch (i++) {
		case 0: libias_test_streq(s, "3"); break;
		}
	}
	libias_test(i == 1);

	libias_test_streq(libias_array_remove(array, 0), "3");
	libias_test(libias_array_len(array) == 0);

	libias_array_set(array, 2, "3");
	libias_array_set(array, 1, "2");
	libias_array_set(array, 0, "1");
	i = 0;
	libias_array_foreach(array, char *, s) {
		libias_test(i == s_index);
		switch (i++) {
		case 0: libias_test_streq(s, "1"); break;
		case 1: libias_test_streq(s, "2"); break;
		case 2: libias_test_streq(s, "3"); break;
		}
	}
	libias_test(i == 3);

	i = 0;
	libias_array_foreach_slice(array, 2, 2, char *, s) {
		i++;
	}
	libias_test(i == 0);

	i = 0;
	libias_array_foreach_slice(array, 20, 0, char *, s) {
		i++;
	}
	libias_test(i == 0);

	i = 0;
	libias_array_foreach_slice(array, 0, 1, char *, s) {
		switch (i++) {
		case 0: libias_test_streq(s, "1"); break;
		}
	}
	libias_test(i == 1);

	i = 0;
	libias_array_foreach_slice(array, 1, 3, char *, s) {
		switch (i++) {
		case 0: libias_test_streq(s, "2"); libias_test(s_index == 1); break;
		case 1: libias_test_streq(s, "3"); libias_test(s_index == 2); break;
		}
	}
	libias_test(i == 2);

	i = 0;
	libias_array_foreach_slice(array, 0, -1, char *, s) {
		switch (i++) {
		case 0: libias_test_streq(s, "1"); libias_test(s_index == 0); break;
		case 1: libias_test_streq(s, "2"); libias_test(s_index == 1); break;
		case 2: libias_test_streq(s, "3"); libias_test(s_index == 2); break;
		}
	}
	libias_test(i == 3);

	i = 0;
	libias_array_foreach_slice(array, 0, -2, char *, s) {
		switch (i++) {
		case 0: libias_test_streq(s, "1"); libias_test(s_index == 0); break;
		case 1: libias_test_streq(s, "2"); libias_test(s_index == 1); break;
		}
	}
	libias_test(i == 2);

	i = 0;
	libias_array_foreach_slice(array, 0, -3, char *, s) {
		switch (i++) {
		case 0: libias_test_streq(s, "1"); libias_test(s_index == 0); break;
		}
	}
	libias_test(i == 1);

	i = 0;
	libias_array_foreach_slice(array, 1, -1, char *, s) {
		switch (i++) {
		case 0: libias_test_streq(s, "2"); break;
		case 1: libias_test_streq(s, "3"); break;
		}
	}
	libias_test(i == 2);

	i = 0;
	libias_array_foreach(array, char *, s) {
		break;
	}
	libias_test(i == 0);

	libias_array_insert(array, 0, "0");
	libias_array_insert(array, 4, "4");
	i = 0;
	libias_array_foreach(array, char *, s) {
		libias_test(i == s_index);
		switch (i++) {
		case 0: libias_test_streq(s, "0"); break;
		case 1: libias_test_streq(s, "1"); break;
		case 2: libias_test_streq(s, "2"); break;
		case 3: libias_test_streq(s, "3"); break;
		case 4: libias_test_streq(s, "4"); break;
		}
	}
	libias_test(i == 5);

	libias_scope_array(other);
	libias_array_append(other, "a");
	libias_array_append(other, "b");
	libias_array_join(array, array, other);
	i = 0;
	libias_test_streq(libias_array_remove(array, 9), "4");
	libias_array_foreach(array, char *, s) {
		libias_test(i == s_index);
		switch (i++) {
		case 0: libias_test_streq(s, "0"); break;
		case 1: libias_test_streq(s, "1"); break;
		case 2: libias_test_streq(s, "2"); break;
		case 3: libias_test_streq(s, "3"); break;
		case 4: libias_test_streq(s, "4"); break;
		case 5: libias_test_streq(s, "0"); break;
		case 6: libias_test_streq(s, "1"); break;
		case 7: libias_test_streq(s, "2"); break;
		case 8: libias_test_streq(s, "3"); break;
		case 9: libias_test_streq(s, "a"); break;
		case 10: libias_test_streq(s, "b"); break;
		}
	}
	libias_test(i == 11);

	// Check that ARRAY_FOREACH works with NULL values
	{
		libias_scope_array(array);
		libias_array_append(array, NULL);
		libias_array_append(array, NULL);
		i = 0;
		libias_array_foreach(array, char *, s) {
			i++;
		}
		libias_test(i == 2);

		libias_array_truncate(array);
		for (size_t i = 0; i < 16; i++) {
			libias_array_append(array, "foo");
		}
		libias_array_insert(array, 3, "bar");
	}

	{
		libias_scope_array(array);
		libias_array_set(array, 40, "0");
		libias_array_set(array, 82, "2");
		libias_array_set(array, 92, "1");
		libias_test_streq(libias_array_get(array, 92), "1");
		libias_test_streq(libias_array_get(array, 82), "2");
		libias_test_streq(libias_array_get(array, 40), "0");
	}
}
