// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "iterator.h"
#include "list.h"
#include "mempool.h"
#include "str.h"
#include "test.h"

libias_tests() {
	libias_scope_list(array);
	libias_list_append(array, "1");
	libias_list_append(array, "2");
	libias_list_append(array, "3");
	size_t i = 0;
	libias_list_foreach(array, char *, s) {
		switch (i++) {
		case 0: libias_test_streq(s, "1"); break;
		case 1: libias_test_streq(s, "2"); break;
		case 2: libias_test_streq(s, "3"); break;
		}
	}
	libias_test(i == 3);

	// list_entry_remove() to return the next list entry
	libias_test_streq(libias_list_entry_value(libias_list_entry_remove(libias_list_entry_next(libias_list_head(array)))), "3");
	i = 0;
	libias_list_foreach(array, char *, s) {
		switch (i++) {
		case 0: libias_test_streq(s, "1"); break;
		case 1: libias_test_streq(s, "3"); break;
		}
	}
	libias_test(i == 2);

	libias_test_streq(libias_list_entry_value(libias_list_entry_remove(libias_list_head(array))), "3");
	i = 0;
	libias_list_foreach(array, char *, s) {
		switch (i++) {
		case 0: libias_test_streq(s, "3"); break;
		}
	}
	libias_test(i == 1);

	libias_test(libias_list_entry_remove(libias_list_head(array)) == NULL);
	libias_test(libias_list_len(array) == 0);

	libias_list_append(array, "1");
	libias_list_append(array, "2");
	libias_list_append(array, "3");

	i = 0;
	libias_list_foreach(array, char *, s) {
		switch (i++) {
		case 0: libias_test_streq(s, "1"); break;
		case 1: libias_test_streq(s, "2"); break;
		case 2: libias_test_streq(s, "3"); break;
		}
	}
	libias_test(i == 3);

	i = 0;
	libias_list_foreach_slice(array, 2, 2, char *, s) {
		i++;
	}
	libias_test(i == 0);

	i = 0;
	libias_list_foreach_slice(array, 20, 0, char *, s) {
		i++;
	}
	libias_test(i == 0);

	i = 0;
	libias_list_foreach_slice(array, 0, 1, char *, s) {
		switch (i++) {
		case 0: libias_test_streq(s, "1"); break;
		}
	}
	libias_test(i == 1);

	i = 0;
	libias_list_foreach_slice(array, 1, 3, char *, s) {
		switch (i++) {
		case 0: libias_test_streq(s, "2"); break;
		case 1: libias_test_streq(s, "3"); break;
		}
	}
	libias_test(i == 2);

	i = 0;
	libias_list_foreach_slice(array, 0, -1, char *, s) {
		switch (i++) {
		case 0: libias_test_streq(s, "1"); break;
		case 1: libias_test_streq(s, "2"); break;
		case 2: libias_test_streq(s, "3"); break;
		}
	}
	libias_test(i == 3);

	i = 0;
	libias_list_foreach_slice(array, 0, -2, char *, s) {
		switch (i++) {
		case 0: libias_test_streq(s, "1"); break;
		case 1: libias_test_streq(s, "2"); break;
		}
	}
	libias_test(i == 2);

	i = 0;
	libias_list_foreach_slice(array, 0, -3, char *, s) {
		switch (i++) {
		case 0: libias_test_streq(s, "1"); break;
		}
	}
	libias_test(i == 1);

	i = 0;
	libias_list_foreach_slice(array, 1, -1, char *, s) {
		switch (i++) {
		case 0: libias_test_streq(s, "2"); break;
		case 1: libias_test_streq(s, "3"); break;
		}
	}
	libias_test(i == 2);

	i = 0;
	libias_list_foreach(array, char *, s) {
		break;
	}
	libias_test(i == 0);

	libias_list_insert_before(array, libias_list_head(array), "0");
	libias_list_insert_after(array, libias_list_tail(array), "4");
	i = 0;
	libias_list_foreach(array, char *, s) {
		switch (i++) {
		case 0: libias_test_streq(s, "0"); break;
		case 1: libias_test_streq(s, "1"); break;
		case 2: libias_test_streq(s, "2"); break;
		case 3: libias_test_streq(s, "3"); break;
		case 4: libias_test_streq(s, "4"); break;
		}
	}
	libias_test(i == 5);
}
