// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "hashmap.h"
#include "mempool.h"
#include "test.h"
#include "trait/compare.h"
#include "trait/hash.h"
#include "str.h"

libias_tests() {
#if 0
	{
		libias_scope_hashmap(map, libias_str_compare, libias_hash_str);
		libias_hashmap_add(map, "1", "foo");
		libias_hashmap_add(map, "1", "asdf");
		libias_hashmap_add(map, "2", "bar");

		libias_test(libias_hashmap_len(map) == 2);

		libias_test_streq(libias_hashmap_get(map, "1"), "foo");
		libias_test_streq(libias_hashmap_get(map, "2"), "bar");

		libias_hashmap_replace(map, "1", "asdf");
		libias_test(libias_hashmap_len(map) == 2);
		libias_test_streq(libias_hashmap_get(map, "1"), "asdf");

		{
			size_t i = 0;
			libias_hashmap_foreach(map, char *, key, char *, value) {
				i++;
			}
			libias_test(i == libias_hashmap_len(map));
		}

		{
			size_t i = 0;
			libias_hashmap_foreach_slice(map, 1, 2, char *, key, char *, value) {
				i++;
			}
			libias_test(i == (libias_hashmap_len(map) - 1));
		}

		libias_hashmap_remove(map, "2");
		libias_test(libias_hashmap_len(map) == 1);
		libias_test(!libias_hashmap_contains_p(map, "2"));
	}
#endif

	{
		libias_scope_hashmap(map, libias_compare_int64, libias_hash_int64);
		{
			int64_t *i = libias_mempool_alloc(pool, int64_t);
			*i = 1;
			libias_hashmap_add(map, i, "foo");
		}

		{
			int64_t *i = libias_mempool_alloc(pool, int64_t);
			*i = 1;
			libias_hashmap_add(map, i, "asdf");
		}
		{
			int64_t *i = libias_mempool_alloc(pool, int64_t);
			*i = 2;
			libias_hashmap_add(map, i, "bar");
		}

		libias_test(libias_hashmap_len(map) == 2);

		{
			int64_t key = 1;
			libias_test_streq(libias_hashmap_get(map, &key), "foo");
		}

		{
			int64_t key = 2;
			libias_test_streq(libias_hashmap_get(map, &key), "bar");
		}

		{
			int64_t *key = libias_mempool_alloc(pool, int64_t);
			*key = 1;
			libias_hashmap_replace(map, key, "asdf");
			libias_test(libias_hashmap_len(map) == 2);
		}

		{
			int64_t key = 1;
			libias_test_streq(libias_hashmap_get(map, &key), "asdf");
		}

		{
			int64_t key = 2;
			libias_hashmap_remove(map, &key);
			libias_test(libias_hashmap_len(map) == 1);
			libias_test(!libias_hashmap_contains_p(map, &key));
		}
	}

}
