// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "iterator.h"
#include "mempool.h"
#include "queue.h"
#include "str.h"
#include "test.h"

libias_tests() {
	libias_scope_queue(queue);
	libias_queue_push(queue, "1");
	libias_queue_push(queue, "2");
	libias_queue_push(queue, "3");
	libias_test(libias_queue_len(queue) == 3);
	size_t i = 0;
	libias_queue_foreach(queue, const char *, s) {
		libias_test(i == s_index);
		switch (i++) {
		case 0: libias_test_streq(s, "1"); break;
		case 1: libias_test_streq(s, "2"); break;
		case 2: libias_test_streq(s, "3"); break;
		}
	}
	libias_test(i == 3);
	i = 0;
	libias_queue_foreach_slice(queue, 2, -1, const char *, s) {
		switch (i++) {
		case 0: libias_test_streq(s, "3"); break;
		}
	}
	libias_test(i == 1);
	libias_test_streq(libias_queue_peek(queue), "1");
	libias_test_streq(libias_queue_pop(queue), "1");
	libias_test(libias_queue_len(queue) == 2);
	libias_test_streq(libias_queue_pop(queue), "2");
	libias_test(libias_queue_len(queue) == 1);
	libias_test_streq(libias_queue_pop(queue), "3");
	libias_test(libias_queue_len(queue) == 0);
	libias_test(libias_queue_pop(queue) == NULL);

	libias_queue_push(queue, "1");
	libias_queue_push(queue, "2");
	libias_queue_push(queue, "3");
	libias_queue_push(queue, "4");
	libias_test(libias_queue_len(queue) == 4);
	libias_queue_dequeue(queue);
	libias_queue_dequeue(queue);
	libias_queue_dequeue(queue);
	libias_test_streq(libias_queue_peek(queue), "1");
	libias_queue_dequeue(queue);
	libias_test(libias_queue_len(queue) == 0);

	{
		libias_scope_queue(queue);
		libias_queue_push(queue, "1");
		libias_test_streq(libias_queue_dequeue(queue), "1");
		libias_test(libias_queue_pop(queue) == NULL);
	}
}
