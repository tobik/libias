// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "array.h"
#include "mempool.h"
#include "str.h"
#include "test.h"

libias_tests() {
	libias_test(libias_str_suffix_p("foo", "o"));
	libias_test(libias_str_suffix_p("foobarverylong", "verylong"));
	libias_test(!libias_str_suffix_p("foobarverylong", "foobar"));
	libias_test(libias_str_suffix_p("foo", ""));
	libias_test(!libias_str_suffix_p("", "o"));
	libias_test(!libias_str_suffix_p("", "verylong"));
	libias_test(libias_str_suffix_p("", ""));

	struct LibiasArray *array = libias_mempool_array(pool);
	libias_test_streq(libias_mempool_take(pool, libias_str_join(array, ",")), "");
	libias_array_append(array, "1");
	libias_test_streq(libias_mempool_take(pool, libias_str_join( array, ",")), "1");
	libias_array_append(array, "2");
	libias_array_append(array, "3");
	libias_test_streq(libias_mempool_take(pool, libias_str_join( array, ",")), "1,2,3");
	libias_test_streq(libias_mempool_take(pool, libias_str_join( array, "foobar")), "1foobar2foobar3");

	libias_test_streq(libias_mempool_take(pool, libias_str_repeat("foo", 0)), "");
	libias_test_streq(libias_mempool_take(pool, libias_str_repeat("foo", 1)), "foo");
	libias_test_streq(libias_mempool_take(pool, libias_str_repeat("foo", 3)), "foofoofoo");
	libias_test_streq(libias_mempool_take(pool, libias_str_repeat("a", 10)), "aaaaaaaaaa");

	libias_test_streq(libias_mempool_take(pool, libias_str_slice("a", 0, 1)), "a");
	libias_test_streq(libias_mempool_take(pool, libias_str_slice("a", 0, 2)), "a");
	libias_test_streq(libias_mempool_take(pool, libias_str_slice("foo", 0, 2)), "fo");
	libias_test_streq(libias_mempool_take(pool, libias_str_slice("foo", 1, 2)), "o");
	libias_test_streq(libias_mempool_take(pool, libias_str_slice("foo", 2, 1)), "");
	libias_test_streq(libias_mempool_take(pool, libias_str_slice("foo", 1, 3)), "oo");
	libias_test_streq(libias_mempool_take(pool, libias_str_slice("foo", 1, 1000)), "oo");
	libias_test_streq(libias_mempool_take(pool, libias_str_slice("foo", 1000, 1000)), "");
	libias_test_streq(libias_mempool_take(pool, libias_str_slice("foo", 0, -1)), "foo");
	libias_test_streq(libias_mempool_take(pool, libias_str_slice("foo", -1, -1)), "foo");
	libias_test_streq(libias_mempool_take(pool, libias_str_slice("foo", 0, -2)), "fo");
	libias_test_streq(libias_mempool_take(pool, libias_str_slice("foo", 0, -3)), "f");
	libias_test_streq(libias_mempool_take(pool, libias_str_slice("foo", 0, -4)), "");
	libias_test_streq(libias_mempool_take(pool, libias_str_slice("foo", 0, -4)), "");

	libias_test_streq(libias_mempool_take(pool, libias_str_trim("foo")), "foo");
	libias_test_streq(libias_mempool_take(pool, libias_str_trim("   foo  ")), "foo");
	{
		char *prefix = NULL;
		char *suffix = NULL;
		char *trimmed = libias_str_trim_ws("   foo  ", &prefix, &suffix);
		libias_test_streq(trimmed, "foo");
		libias_test_streq(prefix, "   ");
		libias_test_streq(suffix, "  ");
		free(prefix);
		free(suffix);
		free(trimmed);
	}
	{
		char *trimmed = libias_str_trim(" \tfoo  ");
		libias_test_streq(trimmed, "foo");
		free(trimmed);
	}
	{
		char *trimmed = libias_str_triml("foo");
		libias_test_streq(trimmed, "foo");
		free(trimmed);
	}
	{
		char *trimmed = libias_str_triml("   foo  ");
		libias_test_streq(trimmed, "foo  ");
		free(trimmed);
	}
	{
		char *trimmed = libias_str_triml(" \tfoo  ");
		libias_test_streq(trimmed, "foo  ");
		free(trimmed);
	}
	{
		char *prefix = NULL;
		char *trimmed = libias_str_triml_ws(" \tfoo  ", &prefix);
		libias_test_streq(trimmed, "foo  ");
		libias_test_streq(prefix, " \t");
		free(prefix);
		free(trimmed);
	}
	{
		char *trimmed = libias_str_trimr("foo");
		libias_test_streq(trimmed, "foo");
		free(trimmed);
	}
	{
		char *trimmed = libias_str_trimr("   foo  ");
		libias_test_streq(trimmed, "   foo");
		free(trimmed);
	}
	{
		char *trimmed = libias_str_trimr(" \tfoo  ");
		libias_test_streq(trimmed, " \tfoo");
		free(trimmed);
	}
	{
		char *suffix = NULL;
		char *trimmed = libias_str_trimr_ws(" \tfoo  ", &suffix);
		libias_test_streq(trimmed, " \tfoo");
		libias_test_streq(suffix, "  ");
		free(suffix);
		free(trimmed);
	}

	libias_test_streq(libias_mempool_take(pool, libias_str_join(libias_str_split(pool, "foo<<<<bar<<<<baz", "<<<<"), ",")), "foo,bar,baz");
	libias_test_streq(libias_mempool_take(pool, libias_str_join( libias_str_split(pool, "foobar", "foo"), ",")), ",bar");
	libias_test_streq(libias_mempool_take(pool, libias_str_join( libias_str_split(pool, "foo", "abcdefghi"), ",")), "foo");
}
