CSTD = gnu17
LDADD += -lm $LDADD_EXECINFO

types
	LibiasArray
	LibiasBuffer
	LibiasDiff
	LibiasDirectory
	LibiasFile
	LibiasFramemap
	LibiasHashmap
	LibiasHashset
	LibiasIterator
	LibiasList
	LibiasListEntry         no-cleanup
	LibiasMap
	LibiasMempool
	LibiasPath
	LibiasQueue
	LibiasSet
	LibiasStack
	LibiasWorkqueue
	LibiasCompareTrait      no-cleanup
	LibiasHashTrait         no-cleanup

bundle libias.a
	array.c
	buffer.c
	compats.c
	diff.c
	diffutil.c
	framemap.c
	hashmap.c
	hashset.c
	io.c
	io/mkdirp.c
	iterator.c
	libgrapheme/src/case.c
	libgrapheme/src/character.c
	libgrapheme/src/line.c
	libgrapheme/src/sentence.c
	libgrapheme/src/utf8.c
	libgrapheme/src/util.c
	libgrapheme/src/word.c
	list.c
	map.c
	mempool.c
	path.c
	queue.c
	runtime.c
	set.c
	stack.c
	str.c
	trait/compare.c
	trait/hash.c
	util.c
	workqueue.c

tests $builddir/libias.a
	tests/array/array.c
	tests/diff/diffutil.c
	tests/io/lineiterator.c
	tests/list/list.c
	tests/map/framemap.c
	tests/map/hashmap.c
	tests/map/map.c
	tests/path/path.c
	tests/queue/queue.c
	tests/stack/stack.c
	tests/str/str.c
