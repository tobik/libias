// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#include <stdlib.h>

#include "array.h"
#include "map.h"
#include "set.h"

struct LibiasSet {
	struct LibiasMap *map;
};

struct LibiasSetIterator {
	struct LibiasMapIterator *iter;
};

struct LibiasSet *
libias_set_new(struct LibiasCompareTrait *compare)
{
	struct LibiasSet *set = libias_alloc(struct LibiasSet);
	set->map = libias_map_new(compare);
	return set;
}

void
libias_set_cleanup(struct LibiasSet **set_ptr)
{
	struct LibiasSet *set = *set_ptr;
	if (set) {
		libias_cleanup(&set->map);
		libias_free(set);
		*set_ptr = NULL;
	}
}

void
libias_set_add(struct LibiasSet *set, const void *element)
{
	libias_map_add(set->map, element, element);
}

void
libias_set_remove(struct LibiasSet *set, const void *element)
{
	libias_map_remove(set->map, element);
}

void *
libias_set_get(struct LibiasSet *set, const void *element)
{
	return libias_map_get(set->map, element);
}

bool
libias_set_contains_p(struct LibiasSet *set, const void *element)
{
	return libias_map_get(set->map, element) != NULL;
}

size_t
libias_set_len(struct LibiasSet *set)
{
	return libias_map_len(set->map);
}

void
libias_set_truncate(struct LibiasSet *set)
{
	libias_map_truncate(set->map);
}

struct LibiasArray *
libias_set_values(struct LibiasSet *set)
{
	return libias_map_values(set->map);
}

struct LibiasIterator *
libias_set_iterator(struct LibiasSet *set, ssize_t a, ssize_t b)
{
	return libias_map_iterator(set->map, a, b);
}
