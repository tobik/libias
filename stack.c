// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#include <limits.h>
#include <stdlib.h>

#include "iterator.h"
#include "stack.h"
#include "util.h"

struct LibiasStack {
	void **buf;
	size_t cap;
	size_t len;
};

struct LibiasStackIterator {
	struct LibiasStack *stack;
	size_t i;
	size_t len;
};

// Prototypes
static void libias_stack_iterator_cleanup(void **this_ptr);
static bool libias_stack_iterator_next(struct LibiasIterator **it_ptr, size_t *index, void **key, void **value);
static void stack_ensure_capacity(struct LibiasStack *stack, const size_t newlen);

// Constants
static const size_t INITIAL_STACK_CAP = 16;

struct LibiasStack *
libias_stack_new(void)
{
	struct LibiasStack *stack = libias_alloc(struct LibiasStack);
	stack->cap = INITIAL_STACK_CAP;
	stack->len = 0;
	libias_realloc_array(stack->buf, stack->cap);
	return stack;
}

void
libias_stack_cleanup(struct LibiasStack **stack_ptr)
{
	struct LibiasStack *stack = *stack_ptr;
	if (stack) {
		libias_free(stack->buf);
		libias_free(stack);
		*stack_ptr = NULL;
	}
}

size_t
libias_stack_len(struct LibiasStack *stack)
{
	return stack->len;
}

int
libias_stack_contains_p(struct LibiasStack *stack, const void *value)
{
	if (stack->len > 0) {
		for (size_t i = stack->len - 1; ; i--) {
			if (value == stack->buf[i]) {
				return 1;
			}
			if (i == 0) {
				break;
			}
		}
	}
	return 0;
}

void *
libias_stack_peek(struct LibiasStack *stack)
{
	if (stack->len > 0) {
		return stack->buf[stack->len - 1];
	} else {
		return NULL;
	}
}

void *
libias_stack_pop(struct LibiasStack *stack)
{
	if (stack->len > 0) {
		void *v = stack->buf[stack->len - 1];
		stack->len--;
		return v;
	} else {
		return NULL;
	}
}

void
stack_ensure_capacity(struct LibiasStack *stack, const size_t newlen)
{
	if (newlen < stack->cap) {
		return;
	}

	size_t cap = libias_next_power_of_2(newlen);
	if (cap > stack->cap) {
		libias_realloc_array(stack->buf, cap);
		stack->cap = cap;
	}
}

void
libias_stack_push(struct LibiasStack *stack, const void *value)
{
	stack_ensure_capacity(stack, stack->len + 1);
	stack->buf[stack->len++] = (void *)value;
}

void
libias_stack_truncate(struct LibiasStack *stack)
{
	stack->len = 0;
}

struct LibiasIterator *
libias_stack_iterator(struct LibiasStack *stack, ssize_t a, ssize_t b)
{
	struct LibiasStackIterator *this = libias_alloc(struct LibiasStackIterator);
	this->stack = stack;
	libias_slice_to_range(stack->len, a, b, &this->i, &this->len);
	auto it = libias_iterator_new(
		libias_stack_iterator_next,
		this,
		libias_stack_iterator_cleanup);
	return it;
}

void
libias_stack_iterator_cleanup(void **this_ptr)
{
	struct LibiasStackIterator *this = *this_ptr;
	if (this) {
		libias_free(this);
		*this_ptr = NULL;
	}
}

bool
libias_stack_iterator_next(
	struct LibiasIterator **it_ptr,
	size_t *index,
	void **key,
	void **value)
{
	struct LibiasStackIterator *this = libias_iterator_context(*it_ptr);
	if (this->i < this->len) {
		void *v = this->stack->buf[this->len - this->i - 1];
		*index = this->i++;
		*key = v;
		*value = v;
		return true;
	} else {
		return false;
	}
}
