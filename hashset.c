// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#include <stdlib.h>

#include "array.h"
#include "hashmap.h"
#include "hashset.h"

struct LibiasHashset {
	struct LibiasHashmap *map;
};

struct LibiasHashsetIterator {
	struct LibiasHashmapIterator *iter;
};

struct LibiasHashset *
libias_hashset_new(
	struct LibiasCompareTrait *compare,
	struct LibiasHashTrait *hash)
{
	struct LibiasHashset *set = libias_alloc(struct LibiasHashset);
	set->map = libias_hashmap_new(compare, hash);
	return set;
}

void
libias_hashset_cleanup(struct LibiasHashset **set_ptr)
{
	struct LibiasHashset *set = *set_ptr;
	if (set) {
		libias_cleanup(&set->map);
		libias_free(set);
		*set_ptr = NULL;
	}
}

void
libias_hashset_add(struct LibiasHashset *set, const void *element)
{
	libias_hashmap_add(set->map, element, element);
}

void
libias_hashset_remove(struct LibiasHashset *set, const void *element)
{
	libias_hashmap_remove(set->map, element);
}

void *
libias_hashset_get(struct LibiasHashset *set, const void *element)
{
	return libias_hashmap_get(set->map, element);
}

bool
libias_hashset_contains_p(struct LibiasHashset *set, const void *element)
{
	return libias_hashmap_contains_p(set->map, element);
}

size_t
libias_hashset_len(struct LibiasHashset *set)
{
	return libias_hashmap_len(set->map);
}

void
libias_hashset_truncate(struct LibiasHashset *set)
{
	libias_hashmap_truncate(set->map);
}

struct LibiasArray *
libias_hashset_values(struct LibiasHashset *set)
{
	return libias_hashmap_values(set->map);
}

struct LibiasIterator *
libias_hashset_iterator(struct LibiasHashset *set, ssize_t a, ssize_t b)
{
	return libias_hashmap_iterator(set->map, a, b);
}
