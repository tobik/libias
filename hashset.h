// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#pragma once

libias_attr_nonnull(1)
libias_attr_returns_nonnull
struct LibiasHashset *libias_hashset_new(struct LibiasCompareTrait *, struct LibiasHashTrait *);

libias_attr_nonnull(1)
void libias_hashset_cleanup(struct LibiasHashset **);

libias_attr_nonnull(1, 2)
void libias_hashset_add(struct LibiasHashset *, const void *);

libias_attr_nonnull(1, 2)
void libias_hashset_remove(struct LibiasHashset *, const void *);

libias_attr_nonnull(1, 2)
void *libias_hashset_get(struct LibiasHashset *, const void *);

libias_attr_nonnull(1, 2)
bool libias_hashset_contains_p(struct LibiasHashset *, const void *);

libias_attr_nonnull(1)
size_t libias_hashset_len(struct LibiasHashset *);

libias_attr_nonnull(1)
void libias_hashset_truncate(struct LibiasHashset *);

libias_attr_nonnull(1)
libias_attr_returns_nonnull
struct LibiasArray *libias_hashset_values(struct LibiasHashset *);

libias_attr_nonnull(1)
libias_attr_returns_nonnull
struct LibiasIterator *libias_hashset_iterator(struct LibiasHashset *, ssize_t, ssize_t);

#define libias_hashset_foreach_slice(SET, A, B, TYPE, VAR) \
	libias_iterator_foreach(libias_hashset_iterator(SET, A, B), TYPE, VAR, TYPE, libias_macro_gensym(VAR))
#define libias_hashset_foreach(SET, TYPE, VAR) \
	libias_hashset_foreach_slice(SET, 0, -1, TYPE, VAR)

#define libias_scope_hashset(name, cmp, hash) \
	struct LibiasHashset *name libias_attr_cleanup(libias_hashset_cleanup) = libias_hashset_new(cmp, hash)
