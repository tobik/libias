// © Tobias Kortkamp <tobik@FreeBSD.org>
// SPDX-License-Identifier: BSD-2-Clause
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above
//    copyright notice, this list of conditions and the following
//    disclaimer in the documentation and/or other materials provided
//    with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
// OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
// AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "config.h"

#include <stdlib.h>

#include "iterator.h"
#include "mempool.h"

struct LibiasIterator {
	LibiasIteratorNextFn next;
	LibiasIteratorContextCleanupFn context_cleanup;
	void *context;
	struct LibiasMempool *pool;
};

// Prototypes

struct LibiasIterator *
libias_iterator_new(
	LibiasIteratorNextFn next,
	void *context,
	LibiasIteratorContextCleanupFn context_cleanup)
{
	auto it = libias_alloc(struct LibiasIterator);

	it->next = next;
	it->context = context;
	it->context_cleanup = context_cleanup;

	return it;
}

void
libias_iterator_cleanup(struct LibiasIterator **it_ptr)
{
	auto it = *it_ptr;
	if (it) {
		it->context_cleanup(&it->context);
		libias_cleanup(&it->pool);
		libias_free(it);
		*it_ptr = NULL;
	}
}

struct LibiasMempool *
libias_iterator_pool(struct LibiasIterator *it)
{
	if (!it->pool) {
		it->pool = libias_mempool_new();
	}

	return it->pool;
}

bool
libias_iterator_next(
	struct LibiasIterator **it_ptr,
	size_t *index,
	void **key,
	void **value)
{
	bool result = (*it_ptr)->next(it_ptr, index, key, value);
	if (!result) {
		libias_cleanup(it_ptr);
	}
	return result;
}

void *
libias_iterator_context(struct LibiasIterator *it)
{
	return it->context;
}
